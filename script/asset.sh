#!/bin/sh 
cache=$1
env=$2

php ./app/console assets:install --env=$2
php ./app/console assetic:dump --env=$2
if [[ -n "$1" ]] && [[ "${1#*.}" == "cache" ]]; then
	php ./app/console cache:clear --env=$2
fi
