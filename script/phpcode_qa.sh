#!/bin/bash

PHP="env php"
#BASEDIR=$(dirname $0)
#WWWROOTDIR="${BASEDIR}/../"
DATADIR="./data"

if [ -z "$1" ]; then
    SRC="src/"
else
    SRC="$1"
fi

#cd ${WWWROOTDIR}

echo "Starting PHP Code Quality Assurance..."

#echo "******** PHP Mess Detector ********" | tee ${DATADIR}/metrics/phpmd_results.md
#$PHP ./bin/phpmd $SRC text cleancode,codesize,controversial,design,naming,unusedcode | tee -a ${DATADIR}/metrics/phpmd_results.md

#echo "******** PHP Copy/Paste Detector ********" | tee ${DATADIR}/metrics/phpcpd_results.md
#$PHP ./bin/phpcpd $SRC | tee -a ${DATADIR}/metrics/phpcpd_results.md

#echo "******** PHP Depend ********" | tee ${DATADIR}/metrics/phpdepend_results.md
#$PHP ./bin/pdepend --summary-xml=${DATADIR}/metrics/phpdepend_summary.xml --jdepend-chart=${DATADIR}/metrics/phpdepend_depend.svg --overview-pyramid=${DATADIR}/metrics/phpdepend_pyramid.svg $SRC >> ${DATADIR}/metrics/phpdepend_results.md

echo "******** PHP CodeSniffer ********" | tee ${DATADIR}/metrics/phpcs_results.md
$PHP ./bin/phpcs $SRC | tee -a ${DATADIR}/metrics/phpcs_results.md
$PHP ./bin/phpcbf $SRC | tee -a ${DATADIR}/metrics/phpcbf_results.md

echo "Done !"

exit 0;