<?php

namespace App\ApeeBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class FileExtension extends Constraint
{
    public $mimeTypes = '';
    public $message = 'The extension of the file is invalid ("%string%"). Allowed extensions are "%string2%"';

    /**
     * @return string
     */
    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}
