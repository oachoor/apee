<?php

namespace App\ApeeBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class FileExtensionValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!in_array($value->guessClientExtension(), $constraint->mimeTypes)) {
            $this->context->addViolation(
                $constraint->message, ['%string%' => $value->guessClientExtension(),
                                       '%string2%' => implode(',', $constraint->mimeTypes), ]
            );
        }
    }
}
