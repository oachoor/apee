<?php

namespace App\ApeeBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ApeeBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
