<?php

namespace App\ApeeBundle\Controller;

use App\ApeeBundle\Document\DataModel;
use App\ApeeBundle\Document\DataObject;
use App\ApeeBundle\Form\Type\DataObjectType;
use App\ApeeBundle\Form\Type\UploadCSVType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DataObjectController extends Controller
{
    /**
     * Finds and displays a Data Models.
     *
     * @Template()
     */
    public function addManuallyAction(Request $request, DataModel $dataModel)
    {
        $dataObject = new DataObject();
        $dataObject->setDataModel($dataModel);

        $form = $this->createForm(new DataObjectType(), $dataObject);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $save = $this->getDataObjectManager()->saveDataObject($dataObject, $request->request->get('dataObject'));

                if ($save === true) {
                    $this->setSuccess('Data Object successfully added');

                    return $this->redirect($this->generateUrl('data_object_browse', ['id' => $dataModel->getId()]));
                }

                $this->setError($save);
            }
        }

        return ['form' => $form->createView(), 'dataModel' => $dataModel];
    }

    /**
     * Browse Data Objects.
     *
     * @Template()
     */
    public function browseDataAction(DataModel $dataModel)
    {
        return ['dataObjects' => $dataModel->getDataObjects()->toArray(), 'dataModel' => $dataModel];
    }

    /**
     * Upload CSV File.
     *
     * @Template()
     */
    public function uploadCSVAction(Request $request, DataModel $dataModel)
    {
        $form = $this->createForm(new UploadCSVType());

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $data = $form->getData();
                $this->getDataObjectManager()->uploadCSVFile($data);
                $this->setSuccess('CSV File successfully uploaded ');

                return $this->redirect($this->generateUrl('data_object_mapping_csv', ['id' => $dataModel->getId()]));
            }
        }

        return ['form' => $form->createView(), 'dataModel' => $dataModel];
    }

    /**
     * Import Data Objects through CSV.
     *
     * @Template()
     */
    public function mappingCSVAction(Request $request, DataModel $dataModel)
    {
        if ($csvFile = $this->get('session')->get('csv_file')) {
            if ($request->isMethod('POST')) {
                $numRecords = $this->getDataObjectManager()->mappingCSVFile($request, $dataModel, $csvFile);

                if (is_int($numRecords)) {
                    $this->setSuccess($numRecords.' records successfully imported ');

                    return $this->redirect($this->generateUrl('data_object_browse', ['id' => $dataModel->getId()]));
                }

                $this->setError('An Error was occurred during importing the CSV File `'.$numRecords.'` ');
            }

            return ['dataModel' => $dataModel,
                    'csvFileFields' => $this->getDataObjectManager()->getCSVFileFields($csvFile),
                    'dataModelAttributes' => $dataModel->getDataModelAttribute(), ];
        }

        return $this->redirect($this->generateUrl('data_object_upload_csv', ['id' => $dataModel->getId()]));
    }

    /**
     * Remove DataModel.
     *
     * @Template()
     */
    public function removeAction(DataModel $dataModel = null)
    {
        $this->getDataModelManager()->delete($dataModel);

        return $this->redirect($this->generateUrl('data_model_list'));
    }
}
