<?php

namespace App\ApeeBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use App\ApeeBundle\Form\Type\UserType;

class ProviderController extends Controller
{
    /**
     * @Template()
     */
    public function dashboardAction()
    {
        return [];
    }

    /**
     * @Template()
     */
    public function viewProfileAction()
    {
        return ['user' => $this->getCurrentUser()];
    }

    /**
     * @Template()
     *
     * @param Request $request
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editProfileAction(Request $request)
    {
        $user = $this->getCurrentUser();
        $form = $this->createForm(new UserType(), $user);

        if ($request->getMethod() == 'POST') {
            $form->submit($request);

            if ($form->isValid()) {
                $manager = $this->get('doctrine_mongodb')->getManager();
                $manager->persist($user);
                $manager->flush();
                $this->setNotice('Profile changes saved !');

                return $this->redirect($this->generateUrl('provider_edit_profile'));
            }
        }

        return ['form' => $form->createView()];
    }
}
