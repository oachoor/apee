<?php

namespace App\ApeeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use App\ApeeBundle\Document\WebService;
use App\ApeeBundle\Document\DataObject;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations as Rest;

class ApiController extends Controller
{
    /**
     * @Rest\View()
     *
     * @param WebService $webService
     *
     * @var \App\ApeeBundle\Document\DataObject
     * @var \App\ApeeBundle\Document\DataObjectAttributeValue
     *
     * @return array
     */
    public function getObjectListAction(WebService $webService)
    {
        $data = [];
        $objects = $webService->getDataModel()->getDataObjects();

        foreach ($objects as $object) {
            $data[$object->getId()]['id'] = $object->getId();

            $attributesValues = $object->getDataObjectAttributesValue();

            foreach ($attributesValues as $attribute) {
                $data[$object->getId()][$this->getAlias($attribute->getDataModelAttribute()->getName())] = $attribute->getValue();
            }
        }

        return ['objects' => $data];
    }

    /**
     * @Rest\View()
     *
     * @param DataObject $object
     *
     * @var \App\ApeeBundle\Document\DataObjectAttributeValue
     *
     * @return array
     */
    public function getObjectAction(DataObject $object)
    {
        $data = [];
        $data['id'] = $object->getId();

        $attributesValues = $object->getDataObjectAttributesValue();

        foreach ($attributesValues as $attribute) {
            $data[$this->getAlias($attribute->getDataModelAttribute()->getName())] = $attribute->getValue();
        }

        return ['object' => $data];
    }

    /**
     * @Rest\View()
     *
     * @param Request    $request
     * @param WebService $webService
     * @param $objectId
     *
     * @var \App\ApeeBundle\Document\DataModelAttribute
     *
     * @return \FOS\RestBundle\View\View
     */
    public function putWebserviceObjectAction(Request $request, WebService $webService, $objectId)
    {
        $dataObject = $this->getDataObjectManager()->find($objectId);

        try {
            $statusCode = Codes::HTTP_NO_CONTENT;
            $routeOptions = ['webService' => $webService->getId(), 'object' => $dataObject->getId(), '_format' => $request->get('_format')];

            if ($dataObject) {
                $statusCode = Codes::HTTP_ACCEPTED;
                $dataObject->setDataModel($webService->getDataModel());
                $dataAttrValues = [];

                foreach ($webService->getDataModel()->getDataModelAttribute() as $attribute) {
                    if ($request->get($this->getAlias($attribute->getName()))) {
                        $dataAttrValues[$attribute->getId()] = $request->get($this->getAlias($attribute->getName()));
                    }
                }

                $this->getDataObjectManager()->saveDataObject($dataObject, $dataAttrValues);
                $routeOptions = ['webService' => $webService->getId(), 'object' => $dataObject->getId(), '_format' => $request->get('_format')];
            }

            return $this->routeRedirectView('api_1_get_object', $routeOptions, $statusCode);
        } catch (InvalidFormException $exception) {
            return $exception->getForm();
        }
    }

    /**
     * @Rest\Post()
     *
     * @param Request    $request
     * @param WebService $webService
     *
     * @var \App\ApeeBundle\Document\DataModelAttribute
     *
     * @return \FOS\RestBundle\Response
     */
    public function postWebserviceAction(Request $request, WebService $webService)
    {
        try {
            $statusCode = Codes::HTTP_CREATED;
            $dataObject = new DataObject();

            $dataObject->setDataModel($webService->getDataModel());
            $dataAttrValues = [];

            foreach ($webService->getDataModel()->getDataModelAttribute() as $attribute) {
                if ($request->get($this->getAlias($attribute->getName()))) {
                    $dataAttrValues[$attribute->getId()] = $request->get($this->getAlias($attribute->getName()));
                }
            }

            $this->getDataObjectManager()->saveDataObject($dataObject, $dataAttrValues);
            $routeOptions = ['webService' => $webService->getId(), 'object' => $dataObject->getId(), '_format' => $request->get('_format')];

            return $this->routeRedirectView('api_1_get_object', $routeOptions, $statusCode);
        } catch (InvalidFormException $exception) {
            return $exception->getForm();
        }
    }

    /**
     * @Rest\View()
     *
     * @param DataObject $object
     */
    public function deleteObjectAction(DataObject $object)
    {
        $this->getDataObjectManager()->delete($object);
    }
}
