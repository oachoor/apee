<?php

namespace App\ApeeBundle\Controller;

use App\ApeeBundle\Document\DataModel;
use App\ApeeBundle\Document\DataObject;
use App\ApeeBundle\Form\Type\DataObjectType;
use App\ApeeBundle\Form\Type\DataModelType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DataModelController extends Controller
{
    /**
     * Lists all DataModels documents.
     *
     * @Template()
     */
    public function listAction()
    {
        return ['dataModels' => $this->getDataModelManager()->findByUser($this->getCurrentUser())];
    }

    /**
     * Displays a form to create a new DataModel.
     *
     * @Template()
     */
    public function newAction()
    {
        $dataModel = new DataModel();
        $form = $this->createForm(new DataModelType(), $dataModel);

        return ['document' => $dataModel, 'form' => $form->createView()];
    }

    /**
     * @var DataModel
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createAction()
    {
        $dataModel = new DataModel();
        $form = $this->createForm(new DataModelType(), $dataModel);
        $form->submit($this->getRequest());

        if ($form->isValid()) {
            $dataModel = $form->getData();
            $dataModel->setUser($this->getCurrentUser());
            $this->getDataModelManager()->saveDataModel($dataModel);
            $this->setSuccess('Data Model Saved !');

            return $this->redirect($this->generateUrl('data_model_view', ['id' => $dataModel->getId()]));
        }
    }

    /**
     * Finds and displays a DataModel document.
     *
     * @Template()
     */
    public function viewAction(DataModel $dataModel)
    {
        if (!$dataModel) {
            throw $this->createNotFoundException('Unable to find Data Model document.');
        }

        return ['dataModel' => $dataModel];
    }

    /**
     * Finds and displays a DataModel document.
     *
     * @Template()
     */
    public function editAction(Request $request, DataModel $dataModel)
    {
        /*if ( !$dataModel ) {
            throw $this->createNotFoundException('Unable to find Data Model.');
        }*/

        $editForm = $this->createForm(new DataModelType(), $dataModel);

        if ($request->isMethod('POST')) {
            $editForm->handleRequest($request);

            if ($editForm->isValid()) {
                $this->getDataModelManager()->saveDataModel($dataModel);

                return $this->redirect($this->generateUrl('data_model_view', ['id' => $dataModel->getId()]));
            }
        }

        return ['dataModel' => $dataModel, 'form' => $editForm->createView()];
    }

    /**
     * Remove DataModel.
     *
     * @Template()
     */
    public function removeAction(DataModel $dataModel = null)
    {
        $this->getDataModelManager()->delete($dataModel);

        return $this->redirect($this->generateUrl('data_model_list'));
    }

    /**
     * Finds and displays a Data Models.
     *
     * @Template()
     */
    public function dataObjectAddAction(DataModel $dataModel)
    {
        $dataObject = new DataObject();

        $dataObject->setDataModel($dataModel);

        $form = $this->createForm(new DataObjectType(), $dataObject);

        return ['form' => $form->createView(), 'dataModel' => $dataModel];
    }
}
