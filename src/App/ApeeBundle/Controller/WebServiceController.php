<?php

namespace App\ApeeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use App\ApeeBundle\Document\WebService;
use App\ApeeBundle\Form\Type\WebServiceType;

class WebServiceController extends Controller
{
    /**
     * @Template()
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        return ['webServices' => $this->getWebServiceManager()->findByUser($this->getCurrentUser())];
    }

    /**
     * Finds and displays a WebService document.
     *
     * @Template()
     *
     * @param WebService $webService
     *
     * @return array
     */
    public function viewAction(WebService $webService)
    {
        if (!$webService) {
            throw $this->createNotFoundException('Unable to find WebService document.');
        }

        return ['webService' => $webService];
    }

    /**
     * Displays a form to create a new WebService entity.
     *
     * @Template()
     *
     * @param Request $request
     *
     * @var WebService
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function newAction(Request $request)
    {
        $webService = new WebService();
        $form = $this->createForm(new WebServiceType(), $webService, ['current_user' => $this->getCurrentUser()]);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $webService = $form->getData();
                $webService->setUser($this->getCurrentUser());
                $this->getWebServiceManager()->saveWebService($webService);
                $this->setSuccess('Web Service Created with success !');

                $session = $this->getRequest()->getSession();
                $session->set('savedWebService', $webService);

                return $this->redirect($this->generateUrl('webservice_list'));
            }
        }

        return ['document' => $webService, 'form' => $form->createView()];
    }

    /**
     * Edits an existing WebService entity.
     *
     * @Template()
     *
     * @param Request    $request
     * @param WebService $webService
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction(Request $request, WebService $webService)
    {
        if (!$webService) {
            throw $this->createNotFoundException('Unable to find WebService entity.');
        }

        $editForm = $this->createForm(new WebServiceType(), $webService, ['current_user' => $this->getCurrentUser()]);

        if ($request->isMethod('POST')) {
            $editForm->handleRequest($request);

            if ($editForm->isValid()) {
                $this->getWebServiceManager()->saveWebService($webService);

                return $this->redirect($this->generateUrl('webservice_list'));
            }
        }

        return ['webService' => $webService, 'form' => $editForm->createView()];
    }

    /**
     * Remove a WebService entity.
     *
     * @param WebService $webService
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeAction(WebService $webService)
    {
        if (!$webService) {
            throw $this->createNotFoundException('Unable to find WebService entity.');
        }

        $this->getWebServiceManager()->remove($webService);

        return $this->redirect($this->generateUrl('webservice_list'));
    }

    /**
     * Get WebService by id.
     *
     * @param $id
     *
     * @return object
     */
    public function getWebServiceAction($wsId)
    {
        return $this->getWebServiceManager()->find($wsId);
    }
}
