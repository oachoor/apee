<?php

namespace App\ApeeBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController as BaseController;

class Controller extends BaseController
{
    /**
     * @return \Doctrine\Bundle\MongoDBBundle\ManagerRegistry
     */
    public function getDataManager()
    {
        return $this->get('doctrine_mongodb')->getManager();
    }

    /**
     * @return \App\ApeeBundle\Manager\WebServiceManager
     */
    public function getWebServiceManager()
    {
        return $this->get('web_service.manager.webservice');
    }

    /**
     * @return \App\ApeeBundle\Manager\DataModelManager
     */
    public function getDataModelManager()
    {
        return $this->get('web_service.manager.data_model');
    }

    /**
     * @return \App\ApeeBundle\Manager\DataObjectManager
     */
    public function getDataObjectManager()
    {
        return $this->get('web_service.manager.data_object');
    }

    /**
     * @return \App\ApeeBundle\Document\User
     */
    public function getCurrentUser()
    {
        return $this->container->get('security.context')->getToken()->getUser();
    }

    /**
     * @param $msg
     */
    public function setNotice($msg)
    {
        $this->get('session')->getFlashBag()->add('notice', $msg);
    }

    /**
     * @param $msg
     */
    public function setError($msg)
    {
        $this->get('session')->getFlashBag()->add('error', $msg);
    }

    /**
     * @param $msg
     */
    public function setSuccess($msg)
    {
        $this->get('session')->getFlashBag()->add('success', $msg);
    }

    /**
     * @param $string
     */
    public function getAlias($string)
    {
        return str_replace(' ', '_', strtolower($string));
    }
}
