<?php

namespace App\ApeeBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use App\ApeeBundle\Document\DataModelAttributeType;

class LoadDataModelAttributeType implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $attributeTypes = [
            'String',
            'Integer',
            'Float',
            'Boolean',
            'Datetime',
            'Text',
            'Object',
            'Array', ];

        foreach ($attributeTypes as $type) {
            $dataModel = new DataModelAttributeType();
            $dataModel->setType($type);
            $manager->persist($dataModel);
        }

        $manager->flush();
    }
}
