<?php

namespace App\ApeeBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class WebService
{
    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\String
     */
    protected $title;

    /**
     * @MongoDB\String
     */
    protected $description;

    /**
     * @MongoDB\Date
     */
    protected $created;

    /**
     * @MongoDB\Int
     */
    protected $status;

    /**
     * @MongoDB\ReferenceOne(targetDocument="DataModel")
     */
    protected $dataModel;

    /**
     * @MongoDB\ReferenceOne(targetDocument="\App\ApeeBundle\Document\User")
     */
    protected $user;

    /**
     * Get id.
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set created.
     *
     * @param date $created
     *
     * @return self
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return date $created
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set status.
     *
     * @param int $status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return int $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set dataModel.
     *
     * @param App\ApeeBundle\Document\DataModel $dataModel
     *
     * @return self
     */
    public function setDataModel(\App\ApeeBundle\Document\DataModel $dataModel)
    {
        $this->dataModel = $dataModel;

        return $this;
    }

    /**
     * Get dataModel.
     *
     * @return \App\ApeeBundle\Document\DataModel $dataModel
     */
    public function getDataModel()
    {
        return $this->dataModel;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return String
     */
    public function __toString()
    {
        return $this->getTitle();
    }
}
