<?php

namespace App\ApeeBundle\Document;

class CSVFile
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var string
     */
    protected $separator;

    /**
     * @var bool
     */
    protected $ignoreHeader;

    /**
     * @param bool $ignoreHeader
     */
    public function setIgnoreHeader($ignoreHeader)
    {
        $this->ignoreHeader = $ignoreHeader;
    }

    /**
     * @return bool
     */
    public function getIgnoreHeader()
    {
        return $this->ignoreHeader;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }
    /**
     * @return string
     */
    public function getPathName()
    {
        return $this->path.'/'.$this->name;
    }

    /**
     * @param string $separator
     */
    public function setSeparator($separator)
    {
        $this->separator = $separator;
    }

    /**
     * @return string
     */
    public function getSeparator()
    {
        return $this->separator;
    }
}
