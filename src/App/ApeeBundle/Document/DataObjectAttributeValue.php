<?php

namespace App\ApeeBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class DataObjectAttributeValue
{
    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\ReferenceOne(targetDocument="DataObject")
     */
    protected $dataObject;

    /**
     * @MongoDB\ReferenceOne(targetDocument="DataModelAttribute")
     */
    protected $dataModelAttribute;

    /**
     * @MongoDB\ReferenceMany(targetDocument="Language")
     */
    protected $language;

    /**
     * @MongoDB\String
     */
    protected $value;

    /**
     *
     */
    public function __construct()
    {
        $this->language = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dataObject.
     *
     * @param \App\ApeeBundle\Document\DataObject $dataObject
     *
     * @return self
     */
    public function setDataObject(\App\ApeeBundle\Document\DataObject $dataObject)
    {
        $this->dataObject = $dataObject;

        return $this;
    }

    /**
     * Get dataObject.
     *
     * @return \App\ApeeBundle\Document\DataObject $dataObject
     */
    public function getDataObject()
    {
        return $this->dataObject;
    }

    /**
     * Set dataModelAttribute.
     *
     * @param \App\ApeeBundle\Document\DataModelAttribute $dataModelAttribute
     *
     * @return self
     */
    public function setDataModelAttribute(\App\ApeeBundle\Document\DataModelAttribute $dataModelAttribute)
    {
        $this->dataModelAttribute = $dataModelAttribute;

        return $this;
    }

    /**
     * Get dataModelAttribute.
     *
     * @return \App\ApeeBundle\Document\DataModelAttribute $dataModelAttribute
     */
    public function getDataModelAttribute()
    {
        return $this->dataModelAttribute;
    }

    /**
     * Add language.
     *
     * @param \App\ApeeBundle\Document\Language $language
     */
    public function addLanguage(\App\ApeeBundle\Document\Language $language)
    {
        $this->language[] = $language;
    }

    /**
     * Remove language.
     *
     * @param \App\ApeeBundle\Document\Language $language
     */
    public function removeLanguage(\App\ApeeBundle\Document\Language $language)
    {
        $this->language->removeElement($language);
    }

    /**
     * Get language.
     *
     * @return \Doctrine\Common\Collections\Collection $language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set value.
     *
     * @param string $value
     *
     * @return self
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return string $value
     */
    public function getValue()
    {
        return $this->value;
    }

    public function __toString()
    {
        return $this->getId();
    }
}
