<?php

namespace App\ApeeBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class DataModelAttribute
{
    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\String
     */
    protected $name;

    /**
     * @MongoDB\String
     */
    protected $defaultValue;

    /**
     * @MongoDB\Boolean
     */
    protected $required;

    /**
     * @MongoDB\ReferenceOne(targetDocument="DataModelAttributeType")
     */
    protected $dataModelAttributeType;

    /**
     * Get id.
     *
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set defaultValue.
     *
     * @param string $defaultValue
     *
     * @return self
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }

    /**
     * Get defaultValue.
     *
     * @return string $defaultValue
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * Get required.
     *
     * @return bool $required
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * Set required.
     *
     * @param bool $required
     *
     * @return self
     */
    public function setRequired($required)
    {
        $this->required = (boolean) $required;
    }

    /**
     * Set dataModelAttributeType.
     *
     * @param DataModelAttributeType $dataModelAttributeType
     *
     * @return self
     */
    public function setDataModelAttributeType(DataModelAttributeType $dataModelAttributeType)
    {
        $this->dataModelAttributeType = $dataModelAttributeType;

        return $this;
    }

    /**
     * Get dataModelAttributeType.
     *
     * @return \App\ApeeBundle\Document\DataModelAttributeType $dataModelAttribute
     */
    public function getDataModelAttributeType()
    {
        return $this->dataModelAttributeType;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
