<?php

namespace App\ApeeBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class DataObject
{
    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\ReferenceOne(targetDocument="DataModel", inversedBy="dataObjects")
     */
    protected $dataModel;

    /**
     * @MongoDB\ReferenceMany(targetDocument="DataObjectAttributeValue")
     */
    protected $dataObjectAttributesValue;

    /**
     * @MongoDB\ReferenceMany(targetDocument="Language")
     */
    protected $language;

    public function __construct()
    {
        $this->dataObjectAttributesValue = new \Doctrine\Common\Collections\ArrayCollection();
        $this->language = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dataModel.
     *
     * @param \App\ApeeBundle\Document\DataModel $dataModel
     *
     * @return self
     */
    public function setDataModel(\App\ApeeBundle\Document\DataModel $dataModel)
    {
        $this->dataModel = $dataModel;

        return $this;
    }

    /**
     * Get dataModel.
     *
     * @return \App\ApeeBundle\Document\DataModel $dataModel
     */
    public function getDataModel()
    {
        return $this->dataModel;
    }

    /**
     * Add language.
     *
     * @param \App\ApeeBundle\Document\Language $language
     */
    public function addLanguage(\App\ApeeBundle\Document\Language $language)
    {
        $this->language[] = $language;
    }

    /**
     * Remove language.
     *
     * @param \App\ApeeBundle\Document\Language $language
     */
    public function removeLanguage(\App\ApeeBundle\Document\Language $language)
    {
        $this->language->removeElement($language);
    }

    /**
     * Get language.
     *
     * @return \Doctrine\Common\Collections\Collection $language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    public function __toString()
    {
        return (string) $this->getId();
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getDataObjectAttributesValue()
    {
        return $this->dataObjectAttributesValue;
    }

    /**
     * @param mixed $dataObjectAttributesValue
     */
    public function addDataObjectAttributesValue($dataObjectAttributesValue)
    {
        $this->dataObjectAttributesValue[] = $dataObjectAttributesValue;
    }

    /**
     * @param mixed $dataObjectAttributesValue
     */
    public function setDataObjectAttributesValue($dataObjectAttributesValue)
    {
        $this->dataObjectAttributesValue = $dataObjectAttributesValue;
    }

    //    public function getAttributeValue(DataModelAttribute $attribute){
    //        var_dump($attribute->getName());
    //        var_dump($this->getDataObjectAttributesValue()->toArray());
    //        exit;
    //    }
}
