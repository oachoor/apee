<?php

namespace App\ApeeBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class DataModel
{
    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\String
     */
    protected $name;

    /**
     * @MongoDB\String
     */
    protected $description;

    /**
     * @MongoDB\ReferenceOne(targetDocument="\App\ApeeBundle\Document\User")
     */
    protected $user;

    /**
     * @MongoDB\ReferenceMany(targetDocument="DataModelAttribute")
     */
    protected $dataModelAttribute;

    /**
     * @MongoDB\ReferenceMany(targetDocument="DataObject", mappedBy="dataModel")
     */
    protected $dataObjects;

    /**
     * @MongoDB\ReferenceMany(targetDocument="Tag")
     */
    protected $tags;

    public function __construct()
    {
        $this->dataModelAttribute = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dataObjects = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $tags
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection $tags
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $dataObjects
     */
    public function setDataObjects($dataObjects)
    {
        $this->dataObjects = $dataObjects;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection $dataObjects
     */
    public function getDataObjects()
    {
        return $this->dataObjects;
    }

    /**
     * @param \App\ApeeBundle\Document\User $user
     */
    public function setUser(\App\ApeeBundle\Document\User $user)
    {
        $this->user = $user;
    }

    /**
     * @return \App\ApeeBundle\Document\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add dataModelAttribute.
     *
     * @param \App\ApeeBundle\Document\DataModelAttribute $dataModelAttribute
     */
    public function addDataModelAttribute(\App\ApeeBundle\Document\DataModelAttribute $dataModelAttribute)
    {
        $this->dataModelAttribute[] = $dataModelAttribute;
    }

    /**
     * Remove dataModelAttribute.
     *
     * @param \App\ApeeBundle\Document\DataModelAttribute $dataModelAttribute
     */
    public function removeDataModelAttribute(\App\ApeeBundle\Document\DataModelAttribute $dataModelAttribute)
    {
        $this->dataModelAttribute->removeElement($dataModelAttribute);
    }

    /**
     * Add dataObject.
     *
     * @param \App\ApeeBundle\Document\DataObject $dataObject
     */
    public function addDataObjects(\App\ApeeBundle\Document\DataObject $dataObject)
    {
        $this->dataObjects[] = $dataObject;
    }

    /**
     * Remove dataObject.
     *
     * @param \App\ApeeBundle\Document\DataObject $dataObject
     */
    public function removeDataObject(\App\ApeeBundle\Document\DataObject $dataObject)
    {
        $this->dataObjects->removeElement($dataObject);
    }

    /**
     * Add tag.
     *
     * @param \App\ApeeBundle\Document\Tag $tag
     */
    public function addTag(\App\ApeeBundle\Document\Tag $tag)
    {
        $this->tags[] = $tag;
    }

    /**
     * Remove dataModelAttribute.
     *
     * @param \App\ApeeBundle\Document\Tag $tag
     */
    public function removeTag(\App\ApeeBundle\Document\Tag $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Get dataModelAttribute.
     *
     * @return \Doctrine\Common\Collections\Collection $dataModelAttribute
     */
    public function getDataModelAttribute()
    {
        return $this->dataModelAttribute;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }
}
