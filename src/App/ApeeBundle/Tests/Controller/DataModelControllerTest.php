<?php

namespace App\ApeeBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DataModelControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/provider/dashboard');
        $this->assertTrue($crawler->filter('html:contains("Hello Omar")')->count() > 0);
    }
}
