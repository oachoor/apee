<?php

namespace App\ApeeBundle\Manager;

use App\ApeeBundle\Document\User;

class WebServiceManager extends CoreManager
{
    /**
     * @param User $user
     */
    public function findByUser(User $user)
    {
        return $this->getRepository()->createQueryBuilder()->field('user')->references($user)->getQuery()->execute();
    }

    /**
     * @param $webService
     */
    public function saveWebService($webService)
    {
        //$this->em->persist($webService->getDataModel());
        $this->em->persist($webService);
        $this->em->flush();
    }

    /**
     * @param $webService
     */
    public function remove($webService)
    {
        $this->em->remove($webService);
        $this->em->flush();
    }
}
