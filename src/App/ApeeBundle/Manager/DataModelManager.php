<?php

namespace App\ApeeBundle\Manager;

use App\ApeeBundle\Document\User;
use App\ApeeBundle\Document\DataModel;

class DataModelManager extends CoreManager
{
    /**
     * @param User $user
     */
    public function findByUser(User $user)
    {
        return $this->getRepository()->createQueryBuilder()->field('user')->references($user)->getQuery()->execute();
    }

    /**
     * @param DataModel $dataModel
     */
    public function saveDataModel(DataModel $dataModel)
    {
        foreach ($dataModel->getDataModelAttribute() as $attribute) {
            $this->em->persist($attribute);
        }

        foreach ($dataModel->getTags() as $tag) {
            $this->em->persist($tag);
        }

        $this->save($dataModel);
    }

    /**
     * @param DataModel $dataModel
     */
    public function delete(DataModel $dataModel)
    {
        $this->em->remove($dataModel);
        $this->em->flush();
    }

    // public function getDataModelObjects(DataModel $dataModel) {}
}
