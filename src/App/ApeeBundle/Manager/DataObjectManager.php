<?php

namespace App\ApeeBundle\Manager;

use App\ApeeBundle\Document\CSVFile;
use App\ApeeBundle\Document\DataModel;
use App\ApeeBundle\Document\DataModelAttribute;
use App\ApeeBundle\Document\DataObject;
use App\ApeeBundle\Document\DataObjectAttributeValue;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class DataObjectManager extends CoreManager
{
    /**
     * @param DataObject $dataObject
     *
     * @var \App\ApeeBundle\Document\DataModelAttribute
     */
    public function saveDataObject(DataObject $dataObject, $dataAttrValues)
    {
        $dataModelAttrs = $dataObject->getDataModel()->getDataModelAttribute();

        foreach ($dataModelAttrs as $attribute) {
            $attributeId = $attribute->getId();

            if (array_key_exists($attributeId, $dataAttrValues)) {
                $validate = $this->validateValues($attribute, $dataAttrValues[$attributeId]);

                if ($validate !== true) {
                    return $validate;
                }

                $dataAttributeValue = new DataObjectAttributeValue();
                $dataAttributeValue->setDataModelAttribute($attribute);
                $dataAttributeValue->setDataObject($dataObject);
                $dataAttributeValue->setValue($dataAttrValues[$attributeId]);
                $this->em->persist($dataAttributeValue);
                $dataObject->addDataObjectAttributesValue($dataAttributeValue);
            }
        }

        $this->save($dataObject, true);

        return true;
    }

    /**
     * @param DataModelAttribute $dataAttribute
     * @param string             $value
     *
     * @return bool
     */
    protected function validateValues(DataModelAttribute $dataAttribute, $value)
    {
        $success = true;
        $validationArray = [];

        if ($dataAttribute->getRequired()) {
            $validationArray[] = new NotBlank();
        }

        $type = strtolower($dataAttribute->getDataModelAttributeType()->getType());

        if (in_array($type, ['integer', 'float', 'boolean', 'string'])) {
            if ($type == 'integer' || $type == 'float') {
                $type = 'numeric';
            }
            $validateConstraint = new Type(['type' => $type, 'message' => ucfirst($dataAttribute->getName()).' : This value should be of type {{ type }}.']);
            $errorList = $this->validator->validateValue($value, $validateConstraint);

            if (count($errorList) > 0) {
                $success = (string) $errorList[0]->getMessage();
            }
        }

        return $success;
    }

    /**
     * @param array     $data
     * @param DataModel $dataModel
     *
     * @var \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    public function uploadCSVFile(array $data)
    {
        $uploadedFile = $data['file'];
        $headerRow = $data['ignore_first_row'];
        $csvSeparator = $data['separator'];

        if (null === $uploadedFile) {
            return;
        }

        if ($uploadedFile->move($this->getUploadRootDir(), $uploadedFile->getClientOriginalName())) {
            $csvFile = new CSVFile();
            $csvFile->setName($uploadedFile->getClientOriginalName());
            $csvFile->setPath($this->getUploadRootDir());
            $csvFile->setIgnoreHeader($headerRow);
            $csvFile->setSeparator($csvSeparator);
            $this->session->set('csv_file', $csvFile);
        }
    }

    /**
     * @param $csvFile
     *
     * @return array
     */
    public function getCSVFileFields(CSVFile $csvFile)
    {
        $fields = [];
        $handle = fopen($csvFile->getPathName(), 'r');
        if ($handle) {
            $fields = fgetcsv($handle, 1000, $csvFile->getSeparator());
        }

        return $fields;
    }

    /**
     * @return string
     */
    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../web/uploads/csv';
    }

    /**
     * @param Request $request
     */
    public function mappingCSVFile(Request $request, DataModel $dataModel, CSVFile $csvFile)
    {
        $mappedFields = [];
        $modelAttribute = $request->request->get('model_attribute');

        foreach ($modelAttribute as $attribute) {
            $field = $request->request->get('csv_fields_'.$attribute);

            if ($field !== null) {
                $mappedFields[$attribute] = $field;
            }
        }

        if (count($mappedFields) > 0) {
            return $this->importCSVFile($mappedFields, $dataModel, $csvFile);
        }

        return;
    }

    /**
     *
     */
    protected function importCSVFile(array $data, DataModel $dataModel, CSVFile $csvFile)
    {
        $handle = fopen($csvFile->getPathName(), 'r');
        if ($csvFile->getIgnoreHeader() === true) {
            $lineData = fgetcsv($handle, 1000, $csvFile->getSeparator());
        }

        $cRows = 0;

        while (($lineData = fgetcsv($handle, 1000, $csvFile->getSeparator())) !== false) {
            $dataObjectValue = [];

            foreach ($data as $id => $item) {
                if (isset($lineData[$item])) {
                    $dataObjectValue[$id] = $lineData[$item];
                }
            }

            if (count($dataObjectValue) > 0) {
                $dataObject = new DataObject();
                $dataObject->setDataModel($dataModel);
                $saveObject = $this->saveDataObject($dataObject, $dataObjectValue);

                if ($saveObject === true) {
                    $cRows++;
                    continue;
                }

                return $saveObject;
            }
        }

        $this->session->remove('csv_file');

        return $cRows;
    }

    /**
     * @param DataModel $dataModel
     *
     * @return mixed
     */
    public function loadDataObjects(DataModel $dataModel)
    {
        return $this->getRepository()
            ->createQueryBuilder()
            ->field('dataModel')->references($dataModel)
            ->getQuery()
            ->execute();
    }

    /**
     * @param DataObject $dataObject
     */
    public function delete(DataObject $dataObject)
    {
        $this->em->remove($dataObject);
        $this->em->flush();
    }
}
