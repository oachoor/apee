<?php

namespace App\ApeeBundle\Manager;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CoreManager
{
    /**
     * @var \Doctrine\Bundle\MongoDBBundle\ManagerRegistry
     */
    protected $em;

    /**
     * @var \Symfony\Component\HttpFoundation\Session\Session
     */
    protected $session;

    /**
     * @var \Symfony\Component\Validator\Validator
     */
    protected $validator;

    /**
     * @var DocumentRepository
     */
    protected $repository = null;

    /**
     * @param ManagerRegistry    $em
     * @param Session            $session
     * @param ValidatorInterface $validator
     */
    public function __construct(ManagerRegistry $em, Session $session, ValidatorInterface $validator)
    {
        $this->em = $em->getManager();
        $this->session = $session;
        $this->validator = $validator;
    }

    /**
     * @param string $repositoryName
     */
    public function setRepository($repositoryName)
    {
        $this->repository = $this->em->getRepository($repositoryName);
    }

    /**
     * @return DocumentRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @return array
     */
    public function findAll()
    {
        return $this->repository->findAll();
    }

    /**
     * @param $id
     *
     * @return object
     */
    public function find($id)
    {
        return $this->repository->find($id);
    }

    /**
     * @param $entity
     * @param bool $flush
     */
    public function save($entity, $flush = true)
    {
        $this->em->persist($entity);

        if ($flush) {
            $this->em->flush();
        }
    }
}
