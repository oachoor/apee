<?php

namespace App\ApeeBundle\Form\Type;

use App\ApeeBundle\Document\DataModelAttribute;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class DataObjectType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @var \App\ApeeBundle\Document\DataObject
     * @var \App\ApeeBundle\Document\DataModelAttribute
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $self = $this;
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($builder, $self) {
                $form = $event->getForm();
                $data = $event->getData();

                $dataModelAttributes = $data->getDataModel()->getDataModelAttribute();

                foreach ($dataModelAttributes as $attribute) {
                    $options = [];
                    $fieldType = $self->getFieldType($attribute, $options);
                    $defaultOptions = [
                        'mapped' => false,
                    //                        'data' => $attribute->getDefaultValue(),
                        'label' => $attribute->getName(),
                        'constraints' => $self->getConstrains($attribute),
                    ];
                    $options = array_merge($options, $defaultOptions);
                    $form->add($attribute->getId(), $fieldType, $options);
                }

                $form->add('save', 'submit', ['attr' => ['class' => 'simple-button inverse']]);
            }
        );
    }

    /**
     * @param DataModelAttribute $dataAttribute
     *
     * @return string
     */
    public function getFieldType(DataModelAttribute $dataAttribute)
    {
        switch (strtolower($dataAttribute->getDataModelAttributeType()->getType())) {
        case 'float' :
            $fieldType = 'number';
            break;
        case 'integer' :
            $fieldType = 'integer';
            break;
        case 'boolean' :
            $fieldType = 'checkbox';
            break;
        case 'text' :
            $fieldType = 'textarea';
            break;
        default:
            $fieldType = 'text';
        }

        return $fieldType;
    }

    /**
     * @param \App\ApeeBundle\Document\DataModelAttribute $dataAttribute
     *
     * @return array
     */
    public function getConstrains(DataModelAttribute $dataAttribute)
    {
        $validationArray = [];

        if ($dataAttribute->getRequired()) {
            $validationArray[] = new NotBlank();
        }

        $type = strtolower($dataAttribute->getDataModelAttributeType()->getType());

        if (in_array($type, ['integer', 'float', 'boolean', 'string'])) {
            if ($type == 'integer' || $type == 'float') {
                $type = 'numeric';
            }

            $validationArray[] = new Type(['type' => $type, 'message' => ucfirst($dataAttribute->getName()).' : This value should be of type {{ type }}.']);
        }

        return $validationArray;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(['data_class' => 'App\ApeeBundle\Document\DataObject', 'empty_value' => '']);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'dataObject';
    }
}
