<?php

namespace App\ApeeBundle\Form\Type;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class WebServiceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $options['current_user'];
        $builder
            ->add('title')
            ->add('description', 'textarea')
            ->add(
                'dataModel', 'document', ['class' => 'ApeeBundle:DataModel',
                                            'query_builder' => function (DocumentRepository $dr) use ($user) {
                                                return $dr->createQueryBuilder('u')->field('user')->references($user);
                                            },
                ]
            );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(['data_class' => 'App\ApeeBundle\Document\WebService', 'current_user' => null]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'webservice_type';
    }
}
