<?php

namespace App\ApeeBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DataModelAttributeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('defaultValue', null, ['required' => false])
            ->add('required', null, ['required' => false])
            ->add('dataModelAttributeType');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(['data_class' => 'App\ApeeBundle\Document\DataModelAttribute']);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'dataModelAttribute';
    }
}
