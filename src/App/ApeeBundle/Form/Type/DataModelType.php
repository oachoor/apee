<?php

namespace App\ApeeBundle\Form\Type;

use App\ApeeBundle\Document\DataModelAttribute;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DataModelType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description', 'textarea')
            ->add(
                'dataModelAttribute', 'collection', [
                'type' => new DataModelAttributeType(),
                'allow_add' => true,
                'allow_delete' => true,
                ]
            )
            ->add(
                'tags', 'collection', [
                'type' => new TagType(),
                'allow_add' => true,
                'allow_delete' => true,
                ]
            );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(['data_class' => 'App\ApeeBundle\Document\DataModel']);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'dataModel';
    }
}
