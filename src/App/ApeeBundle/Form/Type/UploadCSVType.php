<?php

namespace App\ApeeBundle\Form\Type;

use App\ApeeBundle\Validator\Constraints\FileExtension;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UploadCSVType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', 'file', ['constraints' => [0 => new FileExtension(['mimeTypes' => ['csv']])]])
            ->add('ignore_first_row', 'checkbox', ['label' => 'Ignore first row ?', 'required' => false])
            ->add('separator', 'text', ['attr' => ['class' => 'input-mini'], 'data' => ';'])
            ->add('submit', 'submit', ['attr' => ['class' => 'simple-button inverse']]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    // public function setDefaultOptions(OptionsResolverInterface $resolver){}

    /**
     * @return string
     */
    public function getName()
    {
        return 'import_csv';
    }
}
