******** PHP CodeSniffer ********

FILE: /Users/Oachoor/Sites/Sandbox/src/App/ApeeBundle/ApeeBundle.php
----------------------------------------------------------------------
FOUND 3 ERRORS AFFECTING 3 LINES
----------------------------------------------------------------------
 2 | ERROR | Missing file doc comment
 7 | ERROR | Missing class doc comment
 9 | ERROR | Missing function doc comment
----------------------------------------------------------------------


FILE: ...or/Sites/Sandbox/src/App/ApeeBundle/Controller/ApiController.php
----------------------------------------------------------------------
FOUND 24 ERRORS AND 10 WARNINGS AFFECTING 27 LINES
----------------------------------------------------------------------
   2 | ERROR   | [ ] Missing file doc comment
  19 | ERROR   | [ ] Missing class doc comment
  21 | ERROR   | [ ] Missing short description in doc comment
  23 | ERROR   | [ ] Missing parameter comment
  33 | ERROR   | [ ] Expected "foreach (...) {\n"; found "foreach
     |         |     (...)\n        {\n"
  39 | ERROR   | [ ] Expected "foreach (...) {\n"; found "foreach
     |         |     (...)\n            {\n"
  41 | WARNING | [ ] Line exceeds 85 characters; contains 130
     |         |     characters
  48 | ERROR   | [ ] Missing short description in doc comment
  50 | ERROR   | [ ] Missing parameter comment
  61 | ERROR   | [ ] Expected "foreach (...) {\n"; found "foreach
     |         |     (...)\n        {\n"
  63 | WARNING | [ ] Line exceeds 85 characters; contains 108
     |         |     characters
  69 | ERROR   | [ ] Missing short description in doc comment
  69 | ERROR   | [ ] Doc comment for parameter "$objectId" missing
  71 | ERROR   | [ ] Missing parameter comment
  71 | ERROR   | [x] Expected 4 spaces after parameter type; 1 found
  72 | ERROR   | [ ] Missing parameter comment
  73 | ERROR   | [ ] Missing parameter name
  77 | WARNING | [ ] Line exceeds 85 characters; contains 98
     |         |     characters
  84 | WARNING | [ ] Line exceeds 85 characters; contains 138
     |         |     characters
  93 | ERROR   | [ ] Expected "if (...) {\n"; found "if (...)\n      
     |         |          {\n"
  93 | ERROR   | [x] There must be a single space between the closing
     |         |     parenthesis and the opening brace of a
     |         |     multi-line IF statement; found newline
 101 | ERROR   | [ ] Expected "foreach (...) {\n"; found "foreach
     |         |     (...)\n                {\n"
 101 | WARNING | [ ] Line exceeds 85 characters; contains 92
     |         |     characters
 103 | ERROR   | [ ] Expected "if (...) {\n"; found "if (...)\n      
     |         |                  {\n"
 103 | ERROR   | [x] First condition of a multi-line IF statement
     |         |     must directly follow the opening parenthesis
 103 | WARNING | [ ] Line exceeds 85 characters; contains 89
     |         |     characters
 103 | ERROR   | [x] There must be a single space between the closing
     |         |     parenthesis and the opening brace of a
     |         |     multi-line IF statement; found newline
 105 | WARNING | [ ] Line exceeds 85 characters; contains 132
     |         |     characters
 109 | WARNING | [ ] Line exceeds 85 characters; contains 98
     |         |     characters
 111 | WARNING | [ ] Line exceeds 85 characters; contains 144
     |         |     characters
 115 | WARNING | [ ] Line exceeds 85 characters; contains 103
     |         |     characters
 124 | ERROR   | [ ] Missing short description in doc comment
 126 | ERROR   | [ ] Missing parameter comment
 127 | ERROR   | [ ] Missing @return tag in function comment
----------------------------------------------------------------------
PHPCBF CAN FIX THE 4 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------


FILE: ...choor/Sites/Sandbox/src/App/ApeeBundle/Controller/Controller.php
----------------------------------------------------------------------
FOUND 23 ERRORS AFFECTING 19 LINES
----------------------------------------------------------------------
  2 | ERROR | Missing file doc comment
  7 | ERROR | Missing class doc comment
  9 | ERROR | Missing short description in doc comment
 17 | ERROR | Missing short description in doc comment
 25 | ERROR | Missing short description in doc comment
 33 | ERROR | Missing short description in doc comment
 41 | ERROR | Missing short description in doc comment
 49 | ERROR | Missing short description in doc comment
 49 | ERROR | Doc comment for parameter "$msg" missing
 50 | ERROR | Missing parameter name
 51 | ERROR | Missing @return tag in function comment
 57 | ERROR | Missing short description in doc comment
 57 | ERROR | Doc comment for parameter "$msg" missing
 58 | ERROR | Missing parameter name
 59 | ERROR | Missing @return tag in function comment
 65 | ERROR | Missing short description in doc comment
 65 | ERROR | Doc comment for parameter "$msg" missing
 66 | ERROR | Missing parameter name
 67 | ERROR | Missing @return tag in function comment
 73 | ERROR | Missing short description in doc comment
 73 | ERROR | Doc comment for parameter "$string" missing
 74 | ERROR | Missing parameter name
 75 | ERROR | Missing @return tag in function comment
----------------------------------------------------------------------


FILE: ...es/Sandbox/src/App/ApeeBundle/Controller/DataModelController.php
----------------------------------------------------------------------
FOUND 28 ERRORS AND 4 WARNINGS AFFECTING 26 LINES
----------------------------------------------------------------------
   2 | ERROR   | [ ] Missing file doc comment
  19 | ERROR   | [ ] Missing class doc comment
  23 | ERROR   | [x] There must be exactly one blank line before the
     |         |     tags in a doc comment
  24 | ERROR   | [ ] Missing @return tag in function comment
  27 | WARNING | [ ] Line exceeds 85 characters; contains 99
     |         |     characters
  32 | ERROR   | [x] There must be exactly one blank line before the
     |         |     tags in a doc comment
  33 | ERROR   | [ ] Missing @return tag in function comment
  42 | ERROR   | [ ] Missing function doc comment
  49 | ERROR   | [x] The open comment tag must be the only content on
     |         |     the line
  49 | ERROR   | [ ] Missing short description in doc comment
  49 | ERROR   | [x] The close comment tag must be the only content
     |         |     on the line
  55 | WARNING | [ ] Line exceeds 85 characters; contains 103
     |         |     characters
  59 | ERROR   | [ ] Doc comment for parameter "$dataModel" missing
  61 | ERROR   | [x] There must be exactly one blank line before the
     |         |     tags in a doc comment
  62 | ERROR   | [ ] Missing @return tag in function comment
  66 | WARNING | [ ] Line exceeds 85 characters; contains 88
     |         |     characters
  72 | ERROR   | [ ] Doc comment for parameter "$request" missing
  72 | ERROR   | [ ] Doc comment for parameter "$dataModel" missing
  74 | ERROR   | [x] There must be exactly one blank line before the
     |         |     tags in a doc comment
  75 | ERROR   | [ ] Missing @return tag in function comment
  84 | ERROR   | [ ] Expected "if (...) {\n"; found "if (...)\n      
     |         |      {\n"
  84 | ERROR   | [x] There must be a single space between the closing
     |         |     parenthesis and the opening brace of a
     |         |     multi-line IF statement; found newline
  88 | ERROR   | [ ] Expected "if (...) {\n"; found "if (...)\n      
     |         |          {\n"
  88 | ERROR   | [x] There must be a single space between the closing
     |         |     parenthesis and the opening brace of a
     |         |     multi-line IF statement; found newline
  91 | ERROR   | [x] No space found after comma in function call
  91 | WARNING | [ ] Line exceeds 85 characters; contains 106
     |         |     characters
  98 | ERROR   | [ ] Doc comment for parameter "$dataModel" missing
 100 | ERROR   | [x] There must be exactly one blank line before the
     |         |     tags in a doc comment
 101 | ERROR   | [ ] Missing @return tag in function comment
 108 | ERROR   | [ ] Doc comment for parameter "$dataModel" missing
 110 | ERROR   | [x] There must be exactly one blank line before the
     |         |     tags in a doc comment
 111 | ERROR   | [ ] Missing @return tag in function comment
----------------------------------------------------------------------
PHPCBF CAN FIX THE 11 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------


FILE: ...s/Sandbox/src/App/ApeeBundle/Controller/DataObjectController.php
----------------------------------------------------------------------
FOUND 25 ERRORS AND 10 WARNINGS AFFECTING 28 LINES
----------------------------------------------------------------------
   2 | ERROR   | [ ] Missing file doc comment
  18 | ERROR   | [ ] Missing class doc comment
  20 | ERROR   | [ ] Doc comment for parameter "$request" missing
  20 | ERROR   | [ ] Doc comment for parameter "$dataModel" missing
  22 | ERROR   | [x] There must be exactly one blank line before the
     |         |     tags in a doc comment
  23 | ERROR   | [ ] Missing @return tag in function comment
  31 | ERROR   | [ ] Expected "if (...) {\n"; found "if (...)\n      
     |         |      {\n"
  31 | ERROR   | [x] There must be a single space between the closing
     |         |     parenthesis and the opening brace of a
     |         |     multi-line IF statement; found newline
  35 | ERROR   | [ ] Expected "if (...) {\n"; found "if (...)\n      
     |         |          {\n"
  35 | ERROR   | [x] There must be a single space between the closing
     |         |     parenthesis and the opening brace of a
     |         |     multi-line IF statement; found newline
  37 | WARNING | [ ] Line exceeds 85 characters; contains 121
     |         |     characters
  41 | WARNING | [ ] Line exceeds 85 characters; contains 116
     |         |     characters
  51 | ERROR   | [ ] Doc comment for parameter "$dataModel" missing
  53 | ERROR   | [x] There must be exactly one blank line before the
     |         |     tags in a doc comment
  54 | ERROR   | [ ] Missing @return tag in function comment
  57 | WARNING | [ ] Line exceeds 85 characters; contains 101
     |         |     characters
  60 | ERROR   | [ ] Doc comment for parameter "$request" missing
  60 | ERROR   | [ ] Doc comment for parameter "$dataModel" missing
  62 | ERROR   | [x] There must be exactly one blank line before the
     |         |     tags in a doc comment
  63 | ERROR   | [ ] Missing @return tag in function comment
  74 | WARNING | [ ] Line exceeds 85 characters; contains 117
     |         |     characters
  80 | ERROR   | [ ] Doc comment for parameter "$request" missing
  80 | ERROR   | [ ] Doc comment for parameter "$dataModel" missing
  82 | ERROR   | [x] There must be exactly one blank line before the
     |         |     tags in a doc comment
  83 | ERROR   | [ ] Missing @return tag in function comment
  86 | ERROR   | [ ] Expected "if (...) {\n"; found "if (...)\n      
     |         |      {\n"
  86 | ERROR   | [x] There must be a single space between the closing
     |         |     parenthesis and the opening brace of a
     |         |     multi-line IF statement; found newline
  88 | ERROR   | [ ] Expected "if (...) {\n"; found "if (...)\n      
     |         |          {\n"
  88 | ERROR   | [x] There must be a single space between the closing
     |         |     parenthesis and the opening brace of a
     |         |     multi-line IF statement; found newline
  90 | WARNING | [ ] Line exceeds 85 characters; contains 108
     |         |     characters
  93 | WARNING | [ ] Line exceeds 85 characters; contains 87
     |         |     characters
  94 | WARNING | [ ] Line exceeds 85 characters; contains 116
     |         |     characters
  97 | WARNING | [ ] Line exceeds 85 characters; contains 106
     |         |     characters
 101 | WARNING | [ ] Line exceeds 85 characters; contains 97
     |         |     characters
 105 | WARNING | [ ] Line exceeds 85 characters; contains 108
     |         |     characters
----------------------------------------------------------------------
PHPCBF CAN FIX THE 8 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------


FILE: ...tes/Sandbox/src/App/ApeeBundle/Controller/ProviderController.php
----------------------------------------------------------------------
FOUND 12 ERRORS AFFECTING 10 LINES
----------------------------------------------------------------------
  2 | ERROR | [ ] Missing file doc comment
 11 | ERROR | [ ] Missing class doc comment
 13 | ERROR | [ ] Missing short description in doc comment
 15 | ERROR | [ ] Missing @return tag in function comment
 21 | ERROR | [ ] Missing short description in doc comment
 23 | ERROR | [ ] Missing @return tag in function comment
 29 | ERROR | [ ] Missing short description in doc comment
 31 | ERROR | [ ] Missing parameter comment
 39 | ERROR | [ ] Expected "if (...) {\n"; found "if (...)\n       
    |       |     {\n"
 39 | ERROR | [x] There must be a single space between the closing
    |       |     parenthesis and the opening brace of a multi-line
    |       |     IF statement; found newline
 43 | ERROR | [ ] Expected "if (...) {\n"; found "if (...)\n         
    |       |       {\n"
 43 | ERROR | [x] There must be a single space between the closing
    |       |     parenthesis and the opening brace of a multi-line
    |       |     IF statement; found newline
----------------------------------------------------------------------
PHPCBF CAN FIX THE 2 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------


FILE: ...tes/Sandbox/src/App/ApeeBundle/Controller/SecurityController.php
----------------------------------------------------------------------
FOUND 7 ERRORS AND 4 WARNINGS AFFECTING 8 LINES
----------------------------------------------------------------------
  2 | ERROR   | [ ] Missing file doc comment
  9 | ERROR   | [ ] Missing class doc comment
 11 | ERROR   | [ ] Doc comment for parameter "$request" missing
 13 | ERROR   | [ ] Missing @return tag in function comment
 16 | ERROR   | [ ] Expected "if (...) {\n"; found "if(...){\n"
 16 | ERROR   | [x] First condition of a multi-line IF statement must
    |         |     directly follow the opening parenthesis
 16 | WARNING | [ ] Line exceeds 85 characters; contains 98
    |         |     characters
 16 | ERROR   | [x] There must be a single space between the closing
    |         |     parenthesis and the opening brace of a multi-line
    |         |     IF statement; found 0 spaces
 18 | WARNING | [ ] Line exceeds 85 characters; contains 120
    |         |     characters
 20 | WARNING | [ ] Line exceeds 85 characters; contains 125
    |         |     characters
 21 | WARNING | [ ] Line exceeds 85 characters; contains 110
    |         |     characters
----------------------------------------------------------------------
PHPCBF CAN FIX THE 2 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------


FILE: ...s/Sandbox/src/App/ApeeBundle/Controller/WebServiceController.php
----------------------------------------------------------------------
FOUND 33 ERRORS AND 6 WARNINGS AFFECTING 25 LINES
----------------------------------------------------------------------
   2 | ERROR   | [ ] Missing file doc comment
  14 | ERROR   | [ ] Missing class doc comment
  16 | ERROR   | [ ] Missing short description in doc comment
  22 | WARNING | [ ] Line exceeds 85 characters; contains 101
     |         |     characters
  27 | ERROR   | [x] There must be exactly one blank line before the
     |         |     tags in a doc comment
  28 | ERROR   | [ ] Parameter tags must be grouped together in a doc
     |         |     commment
  28 | ERROR   | [ ] Missing parameter comment
  28 | ERROR   | [x] Tag value indented incorrectly; expected 6
     |         |     spaces but found 1
  29 | ERROR   | [ ] Tag cannot be grouped with parameter tags in a
     |         |     doc comment
  29 | ERROR   | [x] Tag value indented incorrectly; expected 5
     |         |     spaces but found 1
  34 | WARNING | [ ] Line exceeds 85 characters; contains 88
     |         |     characters
  42 | ERROR   | [x] There must be exactly one blank line before the
     |         |     tags in a doc comment
  43 | ERROR   | [ ] Parameter tags must be grouped together in a doc
     |         |     commment
  43 | ERROR   | [ ] Missing parameter comment
  43 | ERROR   | [x] Tag value indented incorrectly; expected 6
     |         |     spaces but found 1
  44 | ERROR   | [ ] Tag cannot be grouped with parameter tags in a
     |         |     doc comment
  44 | ERROR   | [x] Tag value indented incorrectly; expected 8
     |         |     spaces but found 1
  45 | ERROR   | [ ] Tag cannot be grouped with parameter tags in a
     |         |     doc comment
  45 | ERROR   | [x] Tag value indented incorrectly; expected 5
     |         |     spaces but found 1
  50 | WARNING | [ ] Line exceeds 85 characters; contains 112
     |         |     characters
  52 | ERROR   | [ ] Expected "if (...) {\n"; found "if(...)\n       
     |         |     {\n"
  52 | ERROR   | [x] There must be a single space between the closing
     |         |     parenthesis and the opening brace of a
     |         |     multi-line IF statement; found newline
  56 | ERROR   | [ ] Expected "if (...) {\n"; found "if (...)\n      
     |         |          {\n"
  56 | ERROR   | [x] There must be a single space between the closing
     |         |     parenthesis and the opening brace of a
     |         |     multi-line IF statement; found newline
  75 | ERROR   | [x] There must be exactly one blank line before the
     |         |     tags in a doc comment
  76 | ERROR   | [ ] Parameter tags must be grouped together in a doc
     |         |     commment
  76 | ERROR   | [ ] Missing parameter comment
  76 | ERROR   | [x] Expected 4 spaces after parameter type; 1 found
  76 | ERROR   | [x] Tag value indented incorrectly; expected 6
     |         |     spaces but found 1
  77 | ERROR   | [ ] Missing parameter comment
  77 | ERROR   | [x] Tag value indented incorrectly; expected 6
     |         |     spaces but found 1
  78 | ERROR   | [ ] Tag cannot be grouped with parameter tags in a
     |         |     doc comment
  78 | ERROR   | [x] Tag value indented incorrectly; expected 5
     |         |     spaces but found 1
  83 | WARNING | [ ] Line exceeds 85 characters; contains 86
     |         |     characters
  86 | WARNING | [ ] Line exceeds 85 characters; contains 116
     |         |     characters
 102 | ERROR   | [ ] Missing parameter comment
 108 | WARNING | [ ] Line exceeds 85 characters; contains 86
     |         |     characters
 115 | ERROR   | [ ] Doc comment for parameter "$wsId" missing
 117 | ERROR   | [ ] Missing parameter name
----------------------------------------------------------------------
PHPCBF CAN FIX THE 14 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------


FILE: ...p/ApeeBundle/DataFixtures/MongoDB/LoadDataModelAttributeType.php
----------------------------------------------------------------------
FOUND 5 ERRORS AFFECTING 5 LINES
----------------------------------------------------------------------
  2 | ERROR | Missing file doc comment
  9 | ERROR | Missing class doc comment
 11 | ERROR | Doc comment for parameter "$manager" missing
 13 | ERROR | Missing @return tag in function comment
 26 | ERROR | Expected "foreach (...) {\n"; found "foreach (...)\n   
    |       |     {\n"
----------------------------------------------------------------------


FILE: ...es/Sandbox/src/App/ApeeBundle/DataFixtures/MongoDB/LoadUsers.php
----------------------------------------------------------------------
FOUND 4 ERRORS AFFECTING 4 LINES
----------------------------------------------------------------------
  2 | ERROR | Missing file doc comment
  9 | ERROR | Missing class doc comment
 11 | ERROR | Doc comment for parameter "$manager" missing
 13 | ERROR | Missing @return tag in function comment
----------------------------------------------------------------------


FILE: ...Sandbox/src/App/ApeeBundle/DependencyInjection/ApeeExtension.php
----------------------------------------------------------------------
FOUND 11 ERRORS AND 2 WARNINGS AFFECTING 8 LINES
----------------------------------------------------------------------
  2 | ERROR   | [ ] Missing file doc comment
 13 | WARNING | [ ] Line exceeds 85 characters; contains 91
    |         |     characters
 14 | ERROR   | [ ] Missing @category tag in class comment
 14 | ERROR   | [ ] Missing @package tag in class comment
 14 | ERROR   | [ ] Missing @author tag in class comment
 14 | ERROR   | [ ] Missing @license tag in class comment
 14 | ERROR   | [ ] Missing @link tag in class comment
 17 | ERROR   | [ ] Doc comment for parameter "$configs" missing
 17 | ERROR   | [ ] Doc comment for parameter "$container" missing
 19 | ERROR   | [ ] Missing @return tag in function comment
 22 | ERROR   | [x] Perl-style comments are not allowed. Use "//
    |         |     Comment." or "/* comment */" instead.
 23 | ERROR   | [x] Perl-style comments are not allowed. Use "//
    |         |     Comment." or "/* comment */" instead.
 25 | WARNING | [ ] Line exceeds 85 characters; contains 105
    |         |     characters
----------------------------------------------------------------------
PHPCBF CAN FIX THE 2 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------


FILE: ...Sandbox/src/App/ApeeBundle/DependencyInjection/Configuration.php
----------------------------------------------------------------------
FOUND 8 ERRORS AND 2 WARNINGS AFFECTING 6 LINES
----------------------------------------------------------------------
  2 | ERROR   | [ ] Missing file doc comment
  9 | WARNING | [ ] Line exceeds 85 characters; contains 87
    |         |     characters
 11 | WARNING | [ ] Line exceeds 85 characters; contains 131
    |         |     characters
 12 | ERROR   | [ ] Missing @category tag in class comment
 12 | ERROR   | [ ] Missing @package tag in class comment
 12 | ERROR   | [ ] Missing @author tag in class comment
 12 | ERROR   | [ ] Missing @license tag in class comment
 12 | ERROR   | [ ] Missing @link tag in class comment
 17 | ERROR   | [ ] Missing @return tag in function comment
 21 | ERROR   | [x] Perl-style comments are not allowed. Use "//
    |         |     Comment." or "/* comment */" instead.
----------------------------------------------------------------------
PHPCBF CAN FIX THE 1 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------


FILE: ...rs/Oachoor/Sites/Sandbox/src/App/ApeeBundle/Document/CSVFile.php
----------------------------------------------------------------------
FOUND 23 ERRORS AFFECTING 23 LINES
----------------------------------------------------------------------
  2 | ERROR | Missing file doc comment
  7 | ERROR | Missing class doc comment
  9 | ERROR | Missing short description in doc comment
 14 | ERROR | Missing short description in doc comment
 19 | ERROR | Missing short description in doc comment
 24 | ERROR | Missing short description in doc comment
 29 | ERROR | Missing short description in doc comment
 30 | ERROR | Missing parameter comment
 31 | ERROR | Missing @return tag in function comment
 37 | ERROR | Missing short description in doc comment
 45 | ERROR | Missing short description in doc comment
 46 | ERROR | Missing parameter comment
 47 | ERROR | Missing @return tag in function comment
 53 | ERROR | Missing short description in doc comment
 61 | ERROR | Missing short description in doc comment
 62 | ERROR | Missing parameter comment
 63 | ERROR | Missing @return tag in function comment
 69 | ERROR | Missing short description in doc comment
 76 | ERROR | Missing short description in doc comment
 84 | ERROR | Missing short description in doc comment
 85 | ERROR | Missing parameter comment
 86 | ERROR | Missing @return tag in function comment
 92 | ERROR | Missing short description in doc comment
----------------------------------------------------------------------


FILE: .../Oachoor/Sites/Sandbox/src/App/ApeeBundle/Document/DataModel.php
----------------------------------------------------------------------
FOUND 46 ERRORS AND 3 WARNINGS AFFECTING 43 LINES
----------------------------------------------------------------------
   2 | ERROR   | [ ] Missing file doc comment
   7 | ERROR   | [ ] Missing short description in doc comment
   9 | ERROR   | [ ] Missing @category tag in class comment
   9 | ERROR   | [ ] Missing @package tag in class comment
   9 | ERROR   | [ ] Missing @author tag in class comment
   9 | ERROR   | [ ] Missing @license tag in class comment
   9 | ERROR   | [ ] Missing @link tag in class comment
  12 | ERROR   | [ ] Missing short description in doc comment
  17 | ERROR   | [ ] Missing short description in doc comment
  22 | ERROR   | [ ] Missing short description in doc comment
  27 | ERROR   | [ ] Missing short description in doc comment
  32 | ERROR   | [ ] Missing short description in doc comment
  37 | ERROR   | [ ] Missing short description in doc comment
  42 | ERROR   | [ ] Missing short description in doc comment
  47 | ERROR   | [ ] Missing function doc comment
  49 | WARNING | [ ] Line exceeds 85 characters; contains 87
     |         |     characters
  67 | ERROR   | [ ] Missing parameter comment
  67 | ERROR   | [x] Tag value indented incorrectly; expected 2
     |         |     spaces but found 1
  68 | ERROR   | [ ] Tag cannot be grouped with parameter tags in a
     |         |     doc comment
  89 | ERROR   | [ ] Missing parameter comment
  89 | ERROR   | [x] Tag value indented incorrectly; expected 2
     |         |     spaces but found 1
  90 | ERROR   | [ ] Tag cannot be grouped with parameter tags in a
     |         |     doc comment
 108 | ERROR   | [ ] Missing short description in doc comment
 109 | ERROR   | [ ] Missing parameter comment
 110 | ERROR   | [ ] Missing @return tag in function comment
 116 | ERROR   | [ ] Missing short description in doc comment
 124 | ERROR   | [ ] Missing short description in doc comment
 125 | ERROR   | [ ] Missing parameter comment
 126 | ERROR   | [ ] Missing @return tag in function comment
 132 | ERROR   | [ ] Missing short description in doc comment
 140 | ERROR   | [ ] Missing short description in doc comment
 141 | ERROR   | [ ] Missing parameter comment
 142 | ERROR   | [ ] Missing @return tag in function comment
 148 | ERROR   | [ ] Missing short description in doc comment
 159 | ERROR   | [ ] Missing parameter comment
 160 | ERROR   | [ ] Missing @return tag in function comment
 161 | WARNING | [ ] Line exceeds 85 characters; contains 106
     |         |     characters
 169 | ERROR   | [ ] Missing parameter comment
 170 | ERROR   | [ ] Missing @return tag in function comment
 171 | WARNING | [ ] Line exceeds 85 characters; contains 109
     |         |     characters
 179 | ERROR   | [ ] Missing parameter comment
 180 | ERROR   | [ ] Missing @return tag in function comment
 189 | ERROR   | [ ] Missing parameter comment
 190 | ERROR   | [ ] Missing @return tag in function comment
 199 | ERROR   | [ ] Missing parameter comment
 200 | ERROR   | [ ] Missing @return tag in function comment
 209 | ERROR   | [ ] Missing parameter comment
 210 | ERROR   | [ ] Missing @return tag in function comment
 226 | ERROR   | [ ] Missing short description in doc comment
----------------------------------------------------------------------
PHPCBF CAN FIX THE 2 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------


FILE: ...Sites/Sandbox/src/App/ApeeBundle/Document/DataModelAttribute.php
----------------------------------------------------------------------
FOUND 25 ERRORS AND 1 WARNING AFFECTING 18 LINES
----------------------------------------------------------------------
   2 | ERROR   | [ ] Missing file doc comment
   7 | ERROR   | [ ] Missing short description in doc comment
   9 | ERROR   | [ ] Missing @category tag in class comment
   9 | ERROR   | [ ] Missing @package tag in class comment
   9 | ERROR   | [ ] Missing @author tag in class comment
   9 | ERROR   | [ ] Missing @license tag in class comment
   9 | ERROR   | [ ] Missing @link tag in class comment
  12 | ERROR   | [ ] Missing short description in doc comment
  17 | ERROR   | [ ] Missing short description in doc comment
  22 | ERROR   | [ ] Missing short description in doc comment
  27 | ERROR   | [ ] Missing short description in doc comment
  32 | ERROR   | [ ] Missing short description in doc comment
  50 | ERROR   | [ ] Missing parameter comment
  50 | ERROR   | [x] Tag value indented incorrectly; expected 2
     |         |     spaces but found 1
  51 | ERROR   | [ ] Tag cannot be grouped with parameter tags in a
     |         |     doc comment
  72 | ERROR   | [ ] Missing parameter comment
  72 | ERROR   | [x] Tag value indented incorrectly; expected 2
     |         |     spaces but found 1
  73 | ERROR   | [ ] Tag cannot be grouped with parameter tags in a
     |         |     doc comment
 104 | ERROR   | [ ] Missing parameter comment
 104 | ERROR   | [x] Tag value indented incorrectly; expected 2
     |         |     spaces but found 1
 105 | ERROR   | [ ] Tag cannot be grouped with parameter tags in a
     |         |     doc comment
 115 | ERROR   | [ ] Missing parameter comment
 115 | ERROR   | [x] Tag value indented incorrectly; expected 2
     |         |     spaces but found 1
 116 | ERROR   | [ ] Tag cannot be grouped with parameter tags in a
     |         |     doc comment
 118 | WARNING | [ ] Line exceeds 85 characters; contains 93
     |         |     characters
 134 | ERROR   | [ ] Missing function doc comment
----------------------------------------------------------------------
PHPCBF CAN FIX THE 4 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------


FILE: ...s/Sandbox/src/App/ApeeBundle/Document/DataModelAttributeType.php
----------------------------------------------------------------------
FOUND 13 ERRORS AFFECTING 8 LINES
----------------------------------------------------------------------
  2 | ERROR | [ ] Missing file doc comment
  7 | ERROR | [ ] Missing short description in doc comment
  9 | ERROR | [ ] Missing @category tag in class comment
  9 | ERROR | [ ] Missing @package tag in class comment
  9 | ERROR | [ ] Missing @author tag in class comment
  9 | ERROR | [ ] Missing @license tag in class comment
  9 | ERROR | [ ] Missing @link tag in class comment
 12 | ERROR | [ ] Missing short description in doc comment
 17 | ERROR | [ ] Missing short description in doc comment
 35 | ERROR | [ ] Missing parameter comment
 35 | ERROR | [x] Tag value indented incorrectly; expected 2 spaces
    |       |     but found 1
 36 | ERROR | [ ] Tag cannot be grouped with parameter tags in a doc
    |       |     comment
 54 | ERROR | [ ] Missing function doc comment
----------------------------------------------------------------------
PHPCBF CAN FIX THE 1 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------


FILE: ...Oachoor/Sites/Sandbox/src/App/ApeeBundle/Document/DataObject.php
----------------------------------------------------------------------
FOUND 32 ERRORS AND 1 WARNING AFFECTING 28 LINES
----------------------------------------------------------------------
   2 | ERROR   | [ ] Missing file doc comment
   7 | ERROR   | [ ] Missing short description in doc comment
   9 | ERROR   | [ ] Missing @category tag in class comment
   9 | ERROR   | [ ] Missing @package tag in class comment
   9 | ERROR   | [ ] Missing @author tag in class comment
   9 | ERROR   | [ ] Missing @license tag in class comment
   9 | ERROR   | [ ] Missing @link tag in class comment
  12 | ERROR   | [ ] Missing short description in doc comment
  17 | ERROR   | [ ] Missing short description in doc comment
  22 | ERROR   | [ ] Missing short description in doc comment
  27 | ERROR   | [ ] Missing short description in doc comment
  33 | ERROR   | [ ] Missing function doc comment
  35 | WARNING | [ ] Line exceeds 85 characters; contains 94
     |         |     characters
  52 | ERROR   | [ ] Missing parameter comment
  52 | ERROR   | [x] Tag value indented incorrectly; expected 2
     |         |     spaces but found 1
  53 | ERROR   | [ ] Tag cannot be grouped with parameter tags in a
     |         |     doc comment
  74 | ERROR   | [ ] Missing parameter comment
  75 | ERROR   | [ ] Missing @return tag in function comment
  84 | ERROR   | [ ] Missing parameter comment
  85 | ERROR   | [ ] Missing @return tag in function comment
 101 | ERROR   | [ ] Missing function doc comment
 106 | ERROR   | [ ] Missing short description in doc comment
 114 | ERROR   | [ ] Missing short description in doc comment
 115 | ERROR   | [ ] Missing parameter comment
 116 | ERROR   | [ ] Missing @return tag in function comment
 122 | ERROR   | [ ] Missing short description in doc comment
 123 | ERROR   | [ ] Missing parameter comment
 124 | ERROR   | [ ] Missing @return tag in function comment
 130 | ERROR   | [x] Line indented incorrectly; expected at least 4
     |         |     spaces, found 0
 131 | ERROR   | [x] Line indented incorrectly; expected at least 4
     |         |     spaces, found 0
 132 | ERROR   | [x] Line indented incorrectly; expected at least 4
     |         |     spaces, found 0
 133 | ERROR   | [x] Line indented incorrectly; expected at least 4
     |         |     spaces, found 0
 134 | ERROR   | [x] Line indented incorrectly; expected at least 4
     |         |     spaces, found 0
----------------------------------------------------------------------
PHPCBF CAN FIX THE 6 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------


FILE: ...Sandbox/src/App/ApeeBundle/Document/DataObjectAttributeValue.php
----------------------------------------------------------------------
FOUND 27 ERRORS AND 1 WARNING AFFECTING 21 LINES
----------------------------------------------------------------------
   2 | ERROR   | [ ] Missing file doc comment
   7 | ERROR   | [ ] Missing short description in doc comment
   9 | ERROR   | [ ] Missing @category tag in class comment
   9 | ERROR   | [ ] Missing @package tag in class comment
   9 | ERROR   | [ ] Missing @author tag in class comment
   9 | ERROR   | [ ] Missing @license tag in class comment
   9 | ERROR   | [ ] Missing @link tag in class comment
  12 | ERROR   | [ ] Missing short description in doc comment
  17 | ERROR   | [ ] Missing short description in doc comment
  22 | ERROR   | [ ] Missing short description in doc comment
  27 | ERROR   | [ ] Missing short description in doc comment
  32 | ERROR   | [ ] Missing short description in doc comment
  37 | ERROR   | [ ] Doc comment is empty
  58 | ERROR   | [ ] Missing parameter comment
  58 | ERROR   | [x] Tag value indented incorrectly; expected 2
     |         |     spaces but found 1
  59 | ERROR   | [ ] Tag cannot be grouped with parameter tags in a
     |         |     doc comment
  80 | ERROR   | [ ] Missing parameter comment
  80 | ERROR   | [x] Tag value indented incorrectly; expected 2
     |         |     spaces but found 1
  81 | ERROR   | [ ] Tag cannot be grouped with parameter tags in a
     |         |     doc comment
  83 | WARNING | [ ] Line exceeds 85 characters; contains 106
     |         |     characters
 102 | ERROR   | [ ] Missing parameter comment
 103 | ERROR   | [ ] Missing @return tag in function comment
 112 | ERROR   | [ ] Missing parameter comment
 113 | ERROR   | [ ] Missing @return tag in function comment
 132 | ERROR   | [ ] Missing parameter comment
 132 | ERROR   | [x] Tag value indented incorrectly; expected 2
     |         |     spaces but found 1
 133 | ERROR   | [ ] Tag cannot be grouped with parameter tags in a
     |         |     doc comment
 151 | ERROR   | [ ] Missing function doc comment
----------------------------------------------------------------------
PHPCBF CAN FIX THE 3 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------


FILE: ...s/Oachoor/Sites/Sandbox/src/App/ApeeBundle/Document/Language.php
----------------------------------------------------------------------
FOUND 17 ERRORS AFFECTING 11 LINES
----------------------------------------------------------------------
  2 | ERROR | [ ] Missing file doc comment
  7 | ERROR | [ ] Missing short description in doc comment
  9 | ERROR | [ ] Missing @category tag in class comment
  9 | ERROR | [ ] Missing @package tag in class comment
  9 | ERROR | [ ] Missing @author tag in class comment
  9 | ERROR | [ ] Missing @license tag in class comment
  9 | ERROR | [ ] Missing @link tag in class comment
 12 | ERROR | [ ] Missing short description in doc comment
 17 | ERROR | [ ] Missing short description in doc comment
 22 | ERROR | [ ] Missing short description in doc comment
 40 | ERROR | [ ] Missing parameter comment
 40 | ERROR | [x] Tag value indented incorrectly; expected 2 spaces
    |       |     but found 1
 41 | ERROR | [ ] Tag cannot be grouped with parameter tags in a doc
    |       |     comment
 62 | ERROR | [ ] Missing parameter comment
 62 | ERROR | [x] Tag value indented incorrectly; expected 2 spaces
    |       |     but found 1
 63 | ERROR | [ ] Tag cannot be grouped with parameter tags in a doc
    |       |     comment
 81 | ERROR | [ ] Missing function doc comment
----------------------------------------------------------------------
PHPCBF CAN FIX THE 2 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------


FILE: /Users/Oachoor/Sites/Sandbox/src/App/ApeeBundle/Document/Tag.php
----------------------------------------------------------------------
FOUND 17 ERRORS AFFECTING 13 LINES
----------------------------------------------------------------------
  2 | ERROR | Missing file doc comment
  7 | ERROR | Missing short description in doc comment
  9 | ERROR | Missing @category tag in class comment
  9 | ERROR | Missing @package tag in class comment
  9 | ERROR | Missing @author tag in class comment
  9 | ERROR | Missing @license tag in class comment
  9 | ERROR | Missing @link tag in class comment
 12 | ERROR | Missing short description in doc comment
 17 | ERROR | Missing short description in doc comment
 22 | ERROR | Missing short description in doc comment
 23 | ERROR | Missing parameter comment
 24 | ERROR | Missing @return tag in function comment
 30 | ERROR | Missing short description in doc comment
 38 | ERROR | Missing short description in doc comment
 39 | ERROR | Missing parameter comment
 40 | ERROR | Missing @return tag in function comment
 46 | ERROR | Missing short description in doc comment
----------------------------------------------------------------------


FILE: ...Users/Oachoor/Sites/Sandbox/src/App/ApeeBundle/Document/User.php
----------------------------------------------------------------------
FOUND 39 ERRORS AFFECTING 35 LINES
----------------------------------------------------------------------
   2 | ERROR | Missing file doc comment
   8 | ERROR | Missing short description in doc comment
  10 | ERROR | Missing @category tag in class comment
  10 | ERROR | Missing @package tag in class comment
  10 | ERROR | Missing @author tag in class comment
  10 | ERROR | Missing @license tag in class comment
  10 | ERROR | Missing @link tag in class comment
  13 | ERROR | Missing short description in doc comment
  18 | ERROR | Missing short description in doc comment
  23 | ERROR | Missing short description in doc comment
  28 | ERROR | Missing short description in doc comment
  33 | ERROR | Missing short description in doc comment
  38 | ERROR | Missing short description in doc comment
  43 | ERROR | Missing function doc comment
  49 | ERROR | Missing short description in doc comment
  50 | ERROR | Missing parameter comment
  51 | ERROR | Missing @return tag in function comment
  57 | ERROR | Missing short description in doc comment
  65 | ERROR | Missing short description in doc comment
  66 | ERROR | Missing parameter comment
  67 | ERROR | Missing @return tag in function comment
  73 | ERROR | Missing short description in doc comment
  81 | ERROR | Missing short description in doc comment
  82 | ERROR | Missing parameter comment
  83 | ERROR | Missing @return tag in function comment
  89 | ERROR | Missing short description in doc comment
  97 | ERROR | Missing short description in doc comment
  98 | ERROR | Missing parameter comment
  99 | ERROR | Missing @return tag in function comment
 105 | ERROR | Missing short description in doc comment
 113 | ERROR | Missing short description in doc comment
 114 | ERROR | Missing parameter comment
 115 | ERROR | Missing @return tag in function comment
 121 | ERROR | Missing short description in doc comment
 129 | ERROR | Missing short description in doc comment
 130 | ERROR | Missing parameter comment
 131 | ERROR | Missing @return tag in function comment
 137 | ERROR | Missing short description in doc comment
 145 | ERROR | Missing short description in doc comment
----------------------------------------------------------------------


FILE: ...Oachoor/Sites/Sandbox/src/App/ApeeBundle/Document/WebService.php
----------------------------------------------------------------------
FOUND 34 ERRORS AFFECTING 25 LINES
----------------------------------------------------------------------
   2 | ERROR | [ ] Missing file doc comment
   7 | ERROR | [ ] Missing short description in doc comment
   9 | ERROR | [ ] Missing @category tag in class comment
   9 | ERROR | [ ] Missing @package tag in class comment
   9 | ERROR | [ ] Missing @author tag in class comment
   9 | ERROR | [ ] Missing @license tag in class comment
   9 | ERROR | [ ] Missing @link tag in class comment
  12 | ERROR | [ ] Missing short description in doc comment
  17 | ERROR | [ ] Missing short description in doc comment
  22 | ERROR | [ ] Missing short description in doc comment
  27 | ERROR | [ ] Missing short description in doc comment
  32 | ERROR | [ ] Missing short description in doc comment
  37 | ERROR | [ ] Missing short description in doc comment
  42 | ERROR | [ ] Missing short description in doc comment
  60 | ERROR | [ ] Missing parameter comment
  60 | ERROR | [x] Tag value indented incorrectly; expected 2 spaces
     |       |     but found 1
  61 | ERROR | [ ] Tag cannot be grouped with parameter tags in a doc
     |       |     comment
  82 | ERROR | [ ] Missing parameter comment
  82 | ERROR | [x] Tag value indented incorrectly; expected 2 spaces
     |       |     but found 1
  83 | ERROR | [ ] Tag cannot be grouped with parameter tags in a doc
     |       |     comment
 104 | ERROR | [ ] Missing parameter comment
 104 | ERROR | [x] Tag value indented incorrectly; expected 2 spaces
     |       |     but found 1
 105 | ERROR | [ ] Tag cannot be grouped with parameter tags in a doc
     |       |     comment
 126 | ERROR | [ ] Missing parameter comment
 126 | ERROR | [x] Tag value indented incorrectly; expected 2 spaces
     |       |     but found 1
 127 | ERROR | [ ] Tag cannot be grouped with parameter tags in a doc
     |       |     comment
 148 | ERROR | [ ] Missing parameter comment
 148 | ERROR | [x] Tag value indented incorrectly; expected 2 spaces
     |       |     but found 1
 149 | ERROR | [ ] Tag cannot be grouped with parameter tags in a doc
     |       |     comment
 167 | ERROR | [ ] Missing short description in doc comment
 168 | ERROR | [ ] Missing parameter comment
 169 | ERROR | [ ] Missing @return tag in function comment
 175 | ERROR | [ ] Missing short description in doc comment
 183 | ERROR | [ ] Missing short description in doc comment
----------------------------------------------------------------------
PHPCBF CAN FIX THE 5 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------


FILE: .../Sandbox/src/App/ApeeBundle/Form/Type/DataModelAttributeType.php
----------------------------------------------------------------------
FOUND 10 ERRORS AND 1 WARNING AFFECTING 10 LINES
----------------------------------------------------------------------
  2 | ERROR   | Missing file doc comment
  9 | ERROR   | Missing class doc comment
 11 | ERROR   | Missing short description in doc comment
 11 | ERROR   | Doc comment for parameter "$options" missing
 12 | ERROR   | Missing parameter comment
 13 | ERROR   | Missing @return tag in function comment
 23 | ERROR   | Missing short description in doc comment
 24 | ERROR   | Missing parameter comment
 25 | ERROR   | Missing @return tag in function comment
 28 | WARNING | Line exceeds 85 characters; contains 95 characters
 31 | ERROR   | Missing short description in doc comment
----------------------------------------------------------------------


FILE: ...oor/Sites/Sandbox/src/App/ApeeBundle/Form/Type/DataModelType.php
----------------------------------------------------------------------
FOUND 16 ERRORS AND 1 WARNING AFFECTING 14 LINES
----------------------------------------------------------------------
  2 | ERROR   | [ ] Missing file doc comment
 10 | ERROR   | [ ] Missing class doc comment
 12 | ERROR   | [ ] Missing short description in doc comment
 12 | ERROR   | [ ] Doc comment for parameter "$options" missing
 13 | ERROR   | [ ] Missing parameter comment
 14 | ERROR   | [ ] Missing @return tag in function comment
 20 | ERROR   | [x] Opening parenthesis of a multi-line function call
    |         |     must be the last content on the line
 20 | ERROR   | [x] No space found after comma in function call
 24 | ERROR   | [x] Closing parenthesis of a multi-line function call
    |         |     must be on a line by itself
 25 | ERROR   | [x] Opening parenthesis of a multi-line function call
    |         |     must be the last content on the line
 25 | ERROR   | [x] No space found after comma in function call
 29 | ERROR   | [x] Closing parenthesis of a multi-line function call
    |         |     must be on a line by itself
 32 | ERROR   | [ ] Missing short description in doc comment
 33 | ERROR   | [ ] Missing parameter comment
 34 | ERROR   | [ ] Missing @return tag in function comment
 37 | WARNING | [ ] Line exceeds 85 characters; contains 86
    |         |     characters
 40 | ERROR   | [ ] Missing short description in doc comment
----------------------------------------------------------------------
PHPCBF CAN FIX THE 6 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------


FILE: ...or/Sites/Sandbox/src/App/ApeeBundle/Form/Type/DataObjectType.php
----------------------------------------------------------------------
FOUND 22 ERRORS AND 4 WARNINGS AFFECTING 22 LINES
----------------------------------------------------------------------
   2 | ERROR   | [ ] Missing file doc comment
  18 | ERROR   | [ ] Missing class doc comment
  20 | ERROR   | [ ] Missing short description in doc comment
  21 | ERROR   | [ ] Missing parameter comment
  22 | ERROR   | [ ] Missing parameter comment
  22 | ERROR   | [x] Expected 16 spaces after parameter type; 1 found
  25 | ERROR   | [ ] Missing @return tag in function comment
  35 | WARNING | [ ] Line exceeds 85 characters; contains 86
     |         |     characters
  42 | ERROR   | [x] Line indented incorrectly; expected at least 20
     |         |     spaces, found 0
  42 | ERROR   | [x] Multi-line function call not indented correctly;
     |         |     expected 12 spaces but found 0
  50 | WARNING | [ ] Line exceeds 85 characters; contains 95
     |         |     characters
  55 | ERROR   | [ ] Missing short description in doc comment
  56 | ERROR   | [ ] Missing parameter comment
  62 | ERROR   | [x] Line indented incorrectly; expected 8 spaces,
     |         |     found 12
  81 | ERROR   | [ ] Missing short description in doc comment
  82 | ERROR   | [ ] Missing parameter comment
  95 | ERROR   | [ ] Expected "if (...) {\n"; found "if (...)\n      
     |         |      {\n"
  95 | ERROR   | [x] There must be a single space between the closing
     |         |     parenthesis and the opening brace of a
     |         |     multi-line IF statement; found newline
  97 | ERROR   | [ ] Expected "if (...) {\n"; found "if(...){\n"
  97 | ERROR   | [x] There must be a single space between the closing
     |         |     parenthesis and the opening brace of a
     |         |     multi-line IF statement; found 0 spaces
 101 | WARNING | [ ] Line exceeds 85 characters; contains 157
     |         |     characters
 107 | ERROR   | [ ] Missing short description in doc comment
 108 | ERROR   | [ ] Missing parameter comment
 109 | ERROR   | [ ] Missing @return tag in function comment
 112 | WARNING | [ ] Line exceeds 85 characters; contains 108
     |         |     characters
 115 | ERROR   | [ ] Missing short description in doc comment
----------------------------------------------------------------------
PHPCBF CAN FIX THE 6 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------


FILE: ...s/Oachoor/Sites/Sandbox/src/App/ApeeBundle/Form/Type/TagType.php
----------------------------------------------------------------------
FOUND 10 ERRORS AFFECTING 9 LINES
----------------------------------------------------------------------
  2 | ERROR | Missing file doc comment
  9 | ERROR | Missing class doc comment
 11 | ERROR | Missing short description in doc comment
 11 | ERROR | Doc comment for parameter "$options" missing
 12 | ERROR | Missing parameter comment
 13 | ERROR | Missing @return tag in function comment
 19 | ERROR | Missing short description in doc comment
 20 | ERROR | Missing parameter comment
 21 | ERROR | Missing @return tag in function comment
 27 | ERROR | Missing short description in doc comment
----------------------------------------------------------------------


FILE: ...oor/Sites/Sandbox/src/App/ApeeBundle/Form/Type/UploadCSVType.php
----------------------------------------------------------------------
FOUND 9 ERRORS AND 4 WARNINGS AFFECTING 12 LINES
----------------------------------------------------------------------
  2 | ERROR   | [ ] Missing file doc comment
 10 | ERROR   | [ ] Missing class doc comment
 12 | ERROR   | [ ] Missing short description in doc comment
 12 | ERROR   | [ ] Doc comment for parameter "$options" missing
 13 | ERROR   | [ ] Missing parameter comment
 14 | ERROR   | [ ] Missing @return tag in function comment
 18 | WARNING | [ ] Line exceeds 85 characters; contains 104
    |         |     characters
 19 | WARNING | [ ] Line exceeds 85 characters; contains 105
    |         |     characters
 20 | WARNING | [ ] Line exceeds 85 characters; contains 92
    |         |     characters
 21 | WARNING | [ ] Line exceeds 85 characters; contains 88
    |         |     characters
 24 | ERROR   | [ ] Missing short description in doc comment
 27 | ERROR   | [x] Perl-style comments are not allowed. Use "//
    |         |     Comment." or "/* comment */" instead.
 29 | ERROR   | [ ] Missing short description in doc comment
----------------------------------------------------------------------
PHPCBF CAN FIX THE 1 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------


FILE: .../Oachoor/Sites/Sandbox/src/App/ApeeBundle/Form/Type/UserType.php
----------------------------------------------------------------------
FOUND 11 ERRORS AFFECTING 10 LINES
----------------------------------------------------------------------
  2 | ERROR | [ ] Missing file doc comment
  9 | ERROR | [ ] Missing class doc comment
 11 | ERROR | [ ] Missing short description in doc comment
 12 | ERROR | [ ] Missing parameter comment
 13 | ERROR | [ ] Missing parameter comment
 13 | ERROR | [x] Expected 16 spaces after parameter type; 1 found
 14 | ERROR | [ ] Missing @return tag in function comment
 25 | ERROR | [ ] Missing short description in doc comment
 26 | ERROR | [ ] Missing parameter comment
 27 | ERROR | [ ] Missing @return tag in function comment
 33 | ERROR | [ ] Missing short description in doc comment
----------------------------------------------------------------------
PHPCBF CAN FIX THE 1 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------


FILE: ...or/Sites/Sandbox/src/App/ApeeBundle/Form/Type/WebServiceType.php
----------------------------------------------------------------------
FOUND 15 ERRORS AND 3 WARNINGS AFFECTING 15 LINES
----------------------------------------------------------------------
  2 | ERROR   | [ ] Missing file doc comment
 10 | ERROR   | [ ] Missing class doc comment
 12 | ERROR   | [ ] Missing short description in doc comment
 13 | ERROR   | [ ] Missing parameter comment
 14 | ERROR   | [ ] Missing parameter comment
 14 | ERROR   | [x] Expected 16 spaces after parameter type; 1 found
 15 | ERROR   | [ ] Missing @return tag in function comment
 22 | ERROR   | [x] Opening parenthesis of a multi-line function call
    |         |     must be the last content on the line
 23 | ERROR   | [x] Expected 1 space after FUNCTION keyword; 0 found
 23 | ERROR   | [x] Expected 1 space after closing parenthesis; found
    |         |     0
 23 | WARNING | [ ] Line exceeds 85 characters; contains 110
    |         |     characters
 24 | WARNING | [ ] Line exceeds 85 characters; contains 118
    |         |     characters
 26 | ERROR   | [x] Closing parenthesis of a multi-line function call
    |         |     must be on a line by itself
 29 | ERROR   | [ ] Missing short description in doc comment
 30 | ERROR   | [ ] Missing parameter comment
 31 | ERROR   | [ ] Missing @return tag in function comment
 34 | WARNING | [ ] Line exceeds 85 characters; contains 111
    |         |     characters
 37 | ERROR   | [ ] Missing short description in doc comment
----------------------------------------------------------------------
PHPCBF CAN FIX THE 5 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------


FILE: ...Oachoor/Sites/Sandbox/src/App/ApeeBundle/Manager/CoreManager.php
----------------------------------------------------------------------
FOUND 26 ERRORS AND 1 WARNING AFFECTING 22 LINES
----------------------------------------------------------------------
  2 | ERROR   | [ ] Missing file doc comment
 10 | ERROR   | [ ] Missing class doc comment
 12 | ERROR   | [ ] Missing short description in doc comment
 17 | ERROR   | [ ] Missing short description in doc comment
 22 | ERROR   | [ ] Missing short description in doc comment
 27 | ERROR   | [ ] Missing short description in doc comment
 32 | ERROR   | [ ] Missing short description in doc comment
 33 | ERROR   | [ ] Missing parameter comment
 33 | ERROR   | [x] Expected 4 spaces after parameter type; 1 found
 34 | ERROR   | [ ] Missing parameter comment
 34 | ERROR   | [x] Expected 12 spaces after parameter type; 1 found
 35 | ERROR   | [ ] Missing parameter comment
 37 | WARNING | [ ] Line exceeds 85 characters; contains 94
    |         |     characters
 44 | ERROR   | [ ] Missing short description in doc comment
 45 | ERROR   | [ ] Missing parameter comment
 46 | ERROR   | [ ] Missing @return tag in function comment
 52 | ERROR   | [ ] Missing short description in doc comment
 60 | ERROR   | [ ] Missing short description in doc comment
 68 | ERROR   | [ ] Missing short description in doc comment
 68 | ERROR   | [ ] Doc comment for parameter "$id" missing
 69 | ERROR   | [ ] Missing parameter name
 77 | ERROR   | [ ] Missing short description in doc comment
 77 | ERROR   | [ ] Doc comment for parameter "$entity" missing
 78 | ERROR   | [ ] Missing parameter name
 79 | ERROR   | [ ] Missing parameter comment
 79 | ERROR   | [x] Expected 3 spaces after parameter type; 1 found
 80 | ERROR   | [ ] Missing @return tag in function comment
----------------------------------------------------------------------
PHPCBF CAN FIX THE 3 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------


FILE: ...or/Sites/Sandbox/src/App/ApeeBundle/Manager/DataModelManager.php
----------------------------------------------------------------------
FOUND 14 ERRORS AND 1 WARNING AFFECTING 15 LINES
----------------------------------------------------------------------
  2 | ERROR   | [ ] Missing file doc comment
  9 | ERROR   | [ ] Missing class doc comment
 11 | ERROR   | [ ] Missing short description in doc comment
 12 | ERROR   | [ ] Missing parameter comment
 13 | ERROR   | [ ] Missing @return tag in function comment
 16 | WARNING | [ ] Line exceeds 85 characters; contains 117
    |         |     characters
 19 | ERROR   | [ ] Missing short description in doc comment
 20 | ERROR   | [ ] Missing parameter comment
 21 | ERROR   | [ ] Missing @return tag in function comment
 24 | ERROR   | [ ] Expected "foreach (...) {\n"; found "foreach
    |         |     (...)\n        {\n"
 29 | ERROR   | [ ] Expected "foreach (...) {\n"; found "foreach
    |         |     (...)\n        {\n"
 37 | ERROR   | [ ] Missing short description in doc comment
 38 | ERROR   | [ ] Missing parameter comment
 39 | ERROR   | [ ] Missing @return tag in function comment
 46 | ERROR   | [x] Perl-style comments are not allowed. Use "//
    |         |     Comment." or "/* comment */" instead.
----------------------------------------------------------------------
PHPCBF CAN FIX THE 1 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------


FILE: ...r/Sites/Sandbox/src/App/ApeeBundle/Manager/DataObjectManager.php
----------------------------------------------------------------------
FOUND 49 ERRORS AND 7 WARNINGS AFFECTING 39 LINES
----------------------------------------------------------------------
   2 | ERROR   | [ ] Missing file doc comment
  15 | ERROR   | [ ] Missing class doc comment
  17 | ERROR   | [ ] Missing short description in doc comment
  17 | ERROR   | [ ] Doc comment for parameter "$dataAttributeValues"
     |         |     missing
  18 | ERROR   | [ ] Missing parameter comment
  20 | ERROR   | [ ] Missing @return tag in function comment
  25 | ERROR   | [ ] Expected "foreach (...) {\n"; found "foreach
     |         |     (...)\n        {\n"
  29 | ERROR   | [ ] Expected "if (...) {\n"; found "if (...)\n      
     |         |          {\n"
  29 | ERROR   | [x] There must be a single space between the closing
     |         |     parenthesis and the opening brace of a
     |         |     multi-line IF statement; found newline
  31 | ERROR   | [x] No space found after comma in function call
  31 | WARNING | [ ] Line exceeds 85 characters; contains 97
     |         |     characters
  50 | ERROR   | [ ] Missing short description in doc comment
  51 | ERROR   | [ ] Missing parameter comment
  52 | ERROR   | [ ] Missing parameter comment
  52 | ERROR   | [x] Expected 13 spaces after parameter type; 1 found
  67 | ERROR   | [ ] Expected "if (...) {\n"; found "if(...){\n"
  67 | ERROR   | [x] There must be a single space between the closing
     |         |     parenthesis and the opening brace of a
     |         |     multi-line IF statement; found 0 spaces
  70 | WARNING | [ ] Line exceeds 85 characters; contains 158
     |         |     characters
  71 | WARNING | [ ] Line exceeds 85 characters; contains 86
     |         |     characters
  81 | ERROR   | [ ] Missing short description in doc comment
  82 | ERROR   | [ ] Missing parameter comment
  82 | ERROR   | [x] Expected 5 spaces after parameter type; 1 found
  83 | ERROR   | [ ] Missing parameter comment
  83 | ERROR   | [ ] Superfluous parameter comment
  85 | ERROR   | [ ] Missing @return tag in function comment
  96 | ERROR   | [x] Space after opening parenthesis of function call
     |         |     prohibited
  96 | WARNING | [ ] Line exceeds 85 characters; contains 102
     |         |     characters
 106 | ERROR   | [ ] Missing short description in doc comment
 106 | ERROR   | [ ] Doc comment for parameter "$csvFile" missing
 107 | ERROR   | [ ] Missing parameter name
 120 | ERROR   | [ ] Missing short description in doc comment
 128 | ERROR   | [ ] Missing short description in doc comment
 128 | ERROR   | [ ] Doc comment for parameter "$dataModel" missing
 128 | ERROR   | [ ] Doc comment for parameter "$csvFile" missing
 129 | ERROR   | [ ] Missing parameter comment
 130 | ERROR   | [ ] Missing @return tag in function comment
 131 | WARNING | [ ] Line exceeds 85 characters; contains 92
     |         |     characters
 136 | ERROR   | [ ] Expected "foreach (...) {\n"; found "foreach
     |         |     (...)\n        {\n"
 151 | ERROR   | [ ] Doc comment is empty
 151 | ERROR   | [ ] Doc comment for parameter "$data" missing
 151 | ERROR   | [ ] Doc comment for parameter "$dataModel" missing
 151 | ERROR   | [ ] Doc comment for parameter "$csvFile" missing
 153 | ERROR   | [ ] Missing @return tag in function comment
 154 | WARNING | [ ] Line exceeds 85 characters; contains 89
     |         |     characters
 163 | ERROR   | [ ] Expected "while (...) {\n"; found "while (...)\n
     |         |            {\n"
 163 | WARNING | [ ] Line exceeds 85 characters; contains 88
     |         |     characters
 167 | ERROR   | [ ] Expected "foreach (...) {\n"; found "foreach
     |         |     (...)\n            {\n"
 174 | ERROR   | [ ] Expected "if (...) {\n"; found "if (...)\n      
     |         |          {\n"
 174 | ERROR   | [x] There must be a single space between the closing
     |         |     parenthesis and the opening brace of a
     |         |     multi-line IF statement; found newline
 180 | ERROR   | [ ] Expected "if (...) {\n"; found "if(...){\n"
 180 | ERROR   | [x] There must be a single space between the closing
     |         |     parenthesis and the opening brace of a
     |         |     multi-line IF statement; found 0 spaces
 193 | ERROR   | [ ] Missing short description in doc comment
 194 | ERROR   | [ ] Missing parameter comment
 206 | ERROR   | [ ] Missing short description in doc comment
 207 | ERROR   | [ ] Missing parameter comment
 208 | ERROR   | [ ] Missing @return tag in function comment
----------------------------------------------------------------------
PHPCBF CAN FIX THE 8 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------


FILE: ...r/Sites/Sandbox/src/App/ApeeBundle/Manager/WebServiceManager.php
----------------------------------------------------------------------
FOUND 13 ERRORS AND 1 WARNING AFFECTING 12 LINES
----------------------------------------------------------------------
  2 | ERROR   | Missing file doc comment
  8 | ERROR   | Missing class doc comment
 10 | ERROR   | Missing short description in doc comment
 11 | ERROR   | Missing parameter comment
 12 | ERROR   | Missing @return tag in function comment
 15 | WARNING | Line exceeds 85 characters; contains 117 characters
 18 | ERROR   | Missing short description in doc comment
 18 | ERROR   | Doc comment for parameter "$webService" missing
 19 | ERROR   | Missing parameter name
 20 | ERROR   | Missing @return tag in function comment
 28 | ERROR   | Missing short description in doc comment
 28 | ERROR   | Doc comment for parameter "$webService" missing
 29 | ERROR   | Missing parameter name
 30 | ERROR   | Missing @return tag in function comment
----------------------------------------------------------------------


FILE: ...andbox/src/App/ApeeBundle/Resources/public/css/bootstrap.min.css
----------------------------------------------------------------------
FOUND 0 ERRORS AND 1 WARNING AFFECTING 1 LINE
----------------------------------------------------------------------
 1 | WARNING | File appears to be minified and cannot be processed
----------------------------------------------------------------------


FILE: ...box/src/App/ApeeBundle/Resources/public/css/font-awesome.min.css
----------------------------------------------------------------------
FOUND 0 ERRORS AND 1 WARNING AFFECTING 1 LINE
----------------------------------------------------------------------
 1 | WARNING | File appears to be minified and cannot be processed
----------------------------------------------------------------------


FILE: .../Sites/Sandbox/src/App/ApeeBundle/Resources/public/js/app.min.js
----------------------------------------------------------------------
FOUND 23 ERRORS AFFECTING 21 LINES
----------------------------------------------------------------------
  20 | ERROR | [x] Opening parenthesis of a multi-line function call
     |       |     must be the last content on the line
  22 | ERROR | [x] Closing parenthesis of a multi-line function call
     |       |     must be on a line by itself
  26 | ERROR | [x] Opening parenthesis of a multi-line function call
     |       |     must be the last content on the line
  28 | ERROR | [x] Opening parenthesis of a multi-line function call
     |       |     must be the last content on the line
  30 | ERROR | [x] Closing parenthesis of a multi-line function call
     |       |     must be on a line by itself
  35 | ERROR | [x] Opening parenthesis of a multi-line function call
     |       |     must be the last content on the line
  37 | ERROR | [x] Closing parenthesis of a multi-line function call
     |       |     must be on a line by itself
  38 | ERROR | [x] Closing parenthesis of a multi-line function call
     |       |     must be on a line by itself
  38 | ERROR | [x] Opening parenthesis of a multi-line function call
     |       |     must be the last content on the line
  40 | ERROR | [x] Closing parenthesis of a multi-line function call
     |       |     must be on a line by itself
  40 | ERROR | [x] Opening parenthesis of a multi-line function call
     |       |     must be the last content on the line
  42 | ERROR | [x] Closing parenthesis of a multi-line function call
     |       |     must be on a line by itself
  45 | ERROR | [x] Opening parenthesis of a multi-line function call
     |       |     must be the last content on the line
  52 | ERROR | [x] Closing parenthesis of a multi-line function call
     |       |     must be on a line by itself
  68 | ERROR | [x] Opening parenthesis of a multi-line function call
     |       |     must be the last content on the line
  71 | ERROR | [x] Closing parenthesis of a multi-line function call
     |       |     must be on a line by itself
  83 | ERROR | [x] Opening parenthesis of a multi-line function call
     |       |     must be the last content on the line
  89 | ERROR | [x] Closing parenthesis of a multi-line function call
     |       |     must be on a line by itself
  91 | ERROR | [x] Opening parenthesis of a multi-line function call
     |       |     must be the last content on the line
  93 | ERROR | [x] Closing parenthesis of a multi-line function call
     |       |     must be on a line by itself
 118 | ERROR | [ ] Missing short description in doc comment
 126 | ERROR | [x] Opening parenthesis of a multi-line function call
     |       |     must be the last content on the line
 132 | ERROR | [x] Closing parenthesis of a multi-line function call
     |       |     must be on a line by itself
----------------------------------------------------------------------
PHPCBF CAN FIX THE 22 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------


FILE: .../Sandbox/src/App/ApeeBundle/Resources/public/js/bootstrap.min.js
----------------------------------------------------------------------
FOUND 0 ERRORS AND 1 WARNING AFFECTING 1 LINE
----------------------------------------------------------------------
 1 | WARNING | File appears to be minified and cannot be processed
----------------------------------------------------------------------


FILE: .../Sandbox/src/App/ApeeBundle/Resources/public/js/html5shiv.min.js
----------------------------------------------------------------------
FOUND 0 ERRORS AND 1 WARNING AFFECTING 1 LINE
----------------------------------------------------------------------
 1 | WARNING | File appears to be minified and cannot be processed
----------------------------------------------------------------------


FILE: ...tes/Sandbox/src/App/ApeeBundle/Resources/public/js/jquery.min.js
----------------------------------------------------------------------
FOUND 0 ERRORS AND 1 WARNING AFFECTING 1 LINE
----------------------------------------------------------------------
 1 | WARNING | File appears to be minified and cannot be processed
----------------------------------------------------------------------


FILE: ...rc/App/ApeeBundle/Resources/public/js/jquery.nanoscroller.min.js
----------------------------------------------------------------------
FOUND 0 ERRORS AND 1 WARNING AFFECTING 1 LINE
----------------------------------------------------------------------
 1 | WARNING | File appears to be minified and cannot be processed
----------------------------------------------------------------------


FILE: .../src/App/ApeeBundle/Tests/Controller/DataModelControllerTest.php
----------------------------------------------------------------------
FOUND 3 ERRORS AND 1 WARNING AFFECTING 4 LINES
----------------------------------------------------------------------
  2 | ERROR   | Missing file doc comment
  7 | ERROR   | Missing class doc comment
  9 | ERROR   | Missing function doc comment
 13 | WARNING | Line exceeds 85 characters; contains 88 characters
----------------------------------------------------------------------


FILE: ...ndbox/src/App/ApeeBundle/Validator/Constraints/FileExtension.php
----------------------------------------------------------------------
FOUND 3 ERRORS AND 1 WARNING AFFECTING 4 LINES
----------------------------------------------------------------------
  2 | ERROR   | Missing file doc comment
  7 | ERROR   | Missing class doc comment
 10 | WARNING | Line exceeds 85 characters; contains 110 characters
 12 | ERROR   | Missing short description in doc comment
----------------------------------------------------------------------


FILE: .../App/ApeeBundle/Validator/Constraints/FileExtensionValidator.php
----------------------------------------------------------------------
FOUND 6 ERRORS AND 1 WARNING AFFECTING 5 LINES
----------------------------------------------------------------------
  2 | ERROR   | [ ] Missing file doc comment
  8 | ERROR   | [ ] Missing class doc comment
 10 | ERROR   | [ ] Missing function doc comment
 12 | ERROR   | [x] No space found after comma in function call
 15 | ERROR   | [x] No space found after comma in function call
 15 | ERROR   | [x] Closing parenthesis of a multi-line function call
    |         |     must be on a line by itself
 15 | WARNING | [ ] Line exceeds 85 characters; contains 92
    |         |     characters
----------------------------------------------------------------------
PHPCBF CAN FIX THE 3 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------

Time: 805ms; Memory: 13.75Mb

