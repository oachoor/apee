******** PHP Depend ********
PDepend 2.0.6

Parsing source files:
....................................                            36

Calculating Cyclomatic Complexity metrics:
................                                               321

Calculating Node Loc metrics:
.............                                                  270

Calculating NPath Complexity metrics:
................                                               321

Calculating Inheritance metrics:
..                                                              45

Calculating Node Count metrics:
...........                                                    234

Calculating Hierarchy metrics:
..............                                                 285

Calculating Code Rank metrics:
..                                                              45

Calculating Coupling metrics:
................                                               321

Calculating Class Level metrics:
..............                                                 286

Calculating Cohesion metrics:
......................                                         454

Calculating Dependency metrics:
...........                                                    235

Generating pdepend log files, this may take a moment.

Time: 0:00:01; Memory: 14.25Mb
